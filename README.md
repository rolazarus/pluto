# Here is the repository for Ron and Daves AWS based angular application code name "Pluto"

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.6.
Run `npm install -g @angular/cli` to install the angular CLI.

## Development server

Run `ng serve --open` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. The ``open` flag will automatically open the browser.
Use: `ng serve --port 5005` to run the server on a port other than 4200. Browse to `http://localhost:5005/`

## Build

Run `ng build --prod --aot=true` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build, and the `aot=true` flag for ahead of time compilation (faster loads)

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## NWS Documentation
https://forecast-v3.weather.gov/documentation
https://www.weather.gov/documentation/services-web-api#


## AWS Documentation

Lambdas: https://docs.aws.amazon.com/lambda/latest/dg/welcome.html<br />
API Gateway: https://docs.aws.amazon.com/apigateway/latest/developerguide/welcome.html<br />
DynamoDB: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Introduction.html<br />

## Setting up AWS Account
1. Set up users from IAM 
    - Click users/Add user
    - Give name and password
    - Permissions/add to group OlsenFamily
    - Add tag with user name
    - Login using User Name and account ID
    - :fire: make sure you are logged in at U.S. East - Ohio
2. Set up S3 Static Page
    - Go to S3 service and select 'Create Bucket' (pluto-web)
    - Tags: pluto-Key pluto-Value
    - Permissions: 
        - Block new public ACLs: false
        - Block new public bucket policies: false
        - Block public and cross-account access: false
    - Properties: Set up static website hosting, use defaults.
    - Overview: upload file called index.html, also file called error.html
    - Website link: http://plutoweb.s3-website.us-east-2.amazonaws.com/
3. Set up Lambda function:
    - Services/Lambda
    - Create a function
    - Use a blueprint: microservice-http-endpoint
    - API name: pluto-api
    - Deployment Stage: dev
    - Function name: plutoDynamoDbCrud
    - Role name: plutoLambdaRole
4. Set up DynamoDb table:
    - Services/DynamoDb
    - Table Name: plutoDynamoDbTable
    - Primary Key: plutoDbId
    - Create items manually: Items Tab/create items (firstTestItem)
5. Test Db with Lambda Function:
    - Click Select a test event
    - LambdaToDbTest1.json
6. Test Db with API Gateway:
    - /plutoDynamoDbCrud - Click Resources, ANY
    - Click Test, send a POST request and check to see if it works
7. Deploy API:
    - Go to API Gateway
    - Click on Pluto API
    - Actions -> Deploy API (dev)
8. Test Db with Curl
    - curlToDBTest.txt
    - curl -H "curlToDbTest.txt" "URL of Stage/DbName"
    - terminal:  curl -H "Content-Type: application/json" -X POST -d "{ \"TableName\": \"plutoDynamoDbTable\", \"Item\": {\"plutoDbId\": \"fromCurl1\"}}" https://08stu76o3a.execute-api.us-east-2.amazonaws.com/dev/plutoDynamoDbCrud
    will create the item "fromCurl1" in the DynamoDB table in AWS
9. Enable CORS
    - Go to Lambda function
    - In the Code, add header -> 'Access-Control-Allow-Origin': '*'
10. Test from index.html

## Viewing Lambda logs:
1. Select the Lambda 
2. Click the "Monitoring" tab.
3. Click "View Logs in Cloudwatch" Button
4. Select the log stream

## AWS CLI
AWS CLI Docs: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

**Install on a MAC**

1. Make sure python 2.6.5+ or 3.3+ is installed<br />
*$ python --version* 

2. Download the AWS CLI Bundled Installer from:
https://s3.amazonaws.com/aws-cli/awscli-bundle.zip

3. Run these 3 commands:<br />
*$ curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"*<br />
*$ unzip awscli-bundle.zip*<br />
*$ sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws*<br />

**Set up permissions**<br />
https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html#cli-quick-configuration

1. Go into IAM console and get Access key ID, and Secret Access Key and save them. If you don't know the secret access key, click "create access key" and save it.
2. Enter both keys, and the region "us-east-2" where prompted with the command:<br />
*$ aws configure*

**Copy files into our S3 Bucket**<br />
https://docs.aws.amazon.com/cli/latest/userguide/cli-services-s3-commands.html

<span style="color:red">TODO: We'll need a script or something that just copies the web page we want, not blindly copy the folders.</span>

Copy contents of **the current** folder and all sub-folders<br />
*pluto ron.olsen $ aws s3 sync . s3://plutoweb*

## Installing packages
1. From the working folder, install something with npm: "npm install suncalc"
2. It should install, and add at item to *package.json*
3. Resetup the project via "npm install"
4. Import to a file: *import * as suncalc from 'suncalc';*
5. Done. 

## D3 References
https://d3js.org/
https://github.com/d3/d3/blob/master/API.md
https://www.d3indepth.com/scales/
https://www.tutorialspoint.com/d3js/index.htm

## Other references
https://github.com/mourner/suncalc
https://bl.ocks.org/mbostock/7784f4b2c7838b893e9b  D3 Sun Projection Sample
https://www.mathworks.com/help/matlab/ref/sph2cart.html  coor system calc
https://www.sohamkamani.com/blog/javascript/2019-02-18-d3-geo-projections-explained/
https://github.com/d3/d3-geo-projection
https://www.d3indepth.com/geographic/

note: https://github.com/mourner/suncalc  (sunrise lib)