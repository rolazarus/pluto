import { TestBed } from '@angular/core/testing';

import { PlutoApiService } from './services/pluto-api.service';

describe('PlutoApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlutoApiService = TestBed.get(PlutoApiService);
    expect(service).toBeTruthy();
  });
});
