const ATTRS_TEMPERATURE = {
    text: {
        units: '°F',
        long: 35,
        fill: 'black'
    },
    path: [
        {name: 'stroke', value: 'black'},
        {name: 'stroke-width', value: 1}
    ],
    // "no_" just comments out the circle attributes so it won't render the line ends
    no_circle: [
        {name: 'r', value: 1},
        {name: 'fill', value: 'blue'},
        {name: 'stroke', value: 'blue'},
        {name: 'stroke-width', value: 1}
    ]
};
const ATTRS_DEWPOINT = {
    text: {
        units: '°F',
        long: 35,
        fill: 'green'
    },
    path: [
        {name: 'stroke', value: 'green'},
        {name: 'stroke-width', value: 1}
    ],
    no_circle: [
        {name: 'r', value: 1},
        {name: 'fill', value: 'green'},
        {name: 'stroke', value: 'green'},
        {name: 'stroke-width', value: 1}
    ]
};
const ATTRS_HEATINDEX = {
    text: {
        units: '°F',
        long: 35,
        fill: 'purple'
    },
    path: [
        {name: 'stroke', value: 'purple'},
        {name: 'stroke-width', value: 1}
    ],
    // We want to render the line ends since this data only comes in when applicable
    circle: [
        {name: 'r', value: 1},
        {name: 'fill', value: 'purple'},
        {name: 'stroke', value: 'purple'},
        {name: 'stroke-width', value: 1}
    ]
};
const ATTRS_WINDCHILL = {
    text: {
        units: '°F',
        long: 35,
        fill: 'blue'
    },
    path: [
        {name: 'stroke', value: 'blue'},
        {name: 'stroke-width', value: 1}
    ],
    // "no_" just comments out the circle attributes so it won't render the line ends
    circle: [
        {name: 'r', value: 1},
        {name: 'fill', value: 'blue'},
        {name: 'stroke', value: 'blue'},
        {name: 'stroke-width', value: 1}
    ]
};
const ATTRS_PROBPRECIP = {
    text: {
        units: '%',
        fill: 'green'
    },
    path: [
        {name: 'stroke', value: '#63bf6a'},
        {name: 'stroke-width', value: 1}
    ],
    fill: [
        {name: 'fill', value: '#c2ffc7'},
        {name: 'stroke', value: 'none'},
        {name: 'stroke-width', value: 0},
        {name: 'opacity', value: 0.3}
    ],
    no_circle: [
        {name: 'r', value: 1},
        {name: 'fill', value: '#63bf6a'},
        {name: 'stroke', value: '#63bf6a'},
        {name: 'stroke-width', value: 1}
    ]
};
const ATTRS_SKYCOVER = {
    text: {
        units: '%',
        fill: '#202030'
    },
    path: [
        {name: 'stroke', value: '#202030'},
        {name: 'stroke-width', value: 1}
    ],
    no_circle: [
        {name: 'r', value: 1},
        {name: 'fill', value: '#202030'},
        {name: 'stroke', value: '#202030'},
        {name: 'stroke-width', value: 1}
    ]
};
const ATTRS_RELATIVEHUMIDITY = {
    text: {
        units: '%',
        fill: '#858709'
    },
    path: [
        {name: 'stroke', value: '#C3C60D'},
        {name: 'stroke-width', value: 1}
    ]
};

const ATTRS_WINDGUST = {
    text: {
        units: 'mph',
        fill: '#264d97'
    },
    path: [
        {name: 'stroke', value: '#264d97'},
        {name: 'stroke-width', value: 1}
    ]
};
const ATTRS_WINDSPEED = {
    text: {
        units: 'mph',
        fill: '#101010'
    },
    path: [
        {name: 'stroke', value: '#101010'},
        {name: 'stroke-width', value: 1}
    ]
};
const ATTRS_WINDDIRECTION = {
    path: [
        // note: fill and stroke are calculated from direction
        {name: 'stroke-width', value: 1}
    ]
};

const ATTRS_QUANTITATIVE_PRECIP = {
    path: [ // todo: is this used?
        {name: 'stroke', value: 'green'},
        {name: 'stroke-width', value: 2}
    ],
    poly: [
        {name: 'fill', value: 'lightgreen'},
        {name: 'stroke', value: 'green'},
        {name: 'stroke-width', value: 1},
        {name: 'opacity', value: 1}
    ],
    circle: [
        {name: 'r', value: 1},
        {name: 'fill', value: 'darkgreen'},
        {name: 'stroke', value: 'darkgreen'},
        {name: 'stroke-width', value: 1}
    ]
};

const ATTRS_SNOWFALL_AMOUNT = {
    path: [ // todo: is this used?
        {name: 'stroke', value: 'blue'},
        {name: 'stroke-width', value: 2}
    ],
    poly: [
        {name: 'fill', value: 'lightblue'},
        {name: 'stroke', value: 'blue'},
        {name: 'stroke-width', value: 1},
        {name: 'opacity', value: 1}
    ],
    circle: [
        {name: 'r', value: 1},
        {name: 'fill', value: 'darkblue'},
        {name: 'stroke', value: 'darkblue'},
        {name: 'stroke-width', value: 1}
    ]
};

const ATTRS_CIELINGHEIGHT = {
    text: {
        units: 'ft',
        short: 42,
        long: 47,
        fill: '#A18134'
    },
    path: [
        {name: 'stroke', value: 'orange'},
        {name: 'stroke-width', value: 2}
    ],
    circle: [
        {name: 'r', value: 1},
        {name: 'fill', value: 'orange'},
        {name: 'stroke', value: 'orange'},
        {name: 'stroke-width', value: 1}
    ]
};

// SVG attributes for each chart item
export const NWS_ATTR: any = {
    temperature: ATTRS_TEMPERATURE,
    dewpoint: ATTRS_DEWPOINT,
    heatIndex: ATTRS_HEATINDEX,
    windChill: ATTRS_WINDCHILL,
    probPrecip: ATTRS_PROBPRECIP,
    relativeHumidity: ATTRS_RELATIVEHUMIDITY,
    skyCover: ATTRS_SKYCOVER,
    windGust: ATTRS_WINDGUST,
    windSpeed: ATTRS_WINDSPEED,
    windDirection: ATTRS_WINDDIRECTION,
    ceilingHeight: ATTRS_CIELINGHEIGHT,
    quantPrecip: ATTRS_QUANTITATIVE_PRECIP,
    snowfall: ATTRS_SNOWFALL_AMOUNT
};

export const DAY_AXIS = {
    block: {
        stroke: 'black',
        'stroke-width': 1,
        fillWeekday: '#ffffff',
        fillWeekend: '#c0c0c0'
    }
};

// Colors / Gradients for the daylight background
export const LIGHT: any = {
    night: '#404040',
    dawn: 'url(#dawnGradient)',
    day: '#ffffff',
    dusk: 'url(#duskGradient)',
    opacity: 1
};

export const GRADIENT_DEWPOINT: any = {
    name: 'dewpointGradient',
    belowColor: '#ffffff',
    // minValue is >=, maxValue is <
    stops: [
        {minValue: 50, maxValue: 58, minColor: '#ffffff', maxColor: '#ffff00'},
        {minValue: 58, maxValue: 65, minColor: '#ffff00', maxColor: '#ff8000'},
        {minValue: 65, maxValue: 70, minColor: '#ff8000', maxColor: '#ff0000'}
        // {minValue: 50, maxValue: 55, minColor: '#ffffff', maxColor: '#ffff00'},
        // {minValue: 55, maxValue: 60, minColor: '#ffff00', maxColor: '#80ff00'},
        // {minValue: 60, maxValue: 65, minColor: '#80ff00', maxColor: '#ff8000'},
        // {minValue: 65, maxValue: 70, minColor: '#ff8000', maxColor: '#ff0000'}
    ],
    aboveColor: '#ff0000'
    // TODO: try using colors with same or decreasing FF's
    // #FF0000 - Red
    // #FF8000 - Orange
    // #FFFF00 - Yellow
    // #80FF00 - Yellow/Green
    // #00FF00 - Green
};

export const GRADIENT_WINDCHILL: any = {
    name: 'windchillGradient',
    belowColor: '#9900ff',
    // minValue is >=, maxValue is <
    stops: [
        {minValue: -20, maxValue: 0, minColor: '#9900ff', maxColor: '#0000ff'}, // purple - blue
        {minValue: 0, maxValue: 20, minColor: '#0000ff', maxColor: '#00a2ff'},  // blue - light blue
        {minValue: 20, maxValue: 35, minColor: '#00a2ff', maxColor: '#ffffff'}  // light blue - white
    ],
    aboveColor: '#ffffff'
};

// const CLEAR_SKY = '#bfedff';
// const FULL_CLOUDY = '#89919c';
const CLEAR_SKY = '#e0f6ff';
const FULL_CLOUDY = '#686c6e';
export const GRADIENT_SKYCOVER: any = {
    name: 'skycoverGradient',
    belowColor: CLEAR_SKY,
    // minValue is >=, maxValue is <
    stops: [
        {minValue: 20, maxValue: 90, minColor: CLEAR_SKY, maxColor: FULL_CLOUDY},
    ],
    aboveColor: FULL_CLOUDY
};

const WIND_CALM = '#ffffff';
const WIND_STOP1 = '#329da8';
const WIND_MAX = '#7332a8';
export const GRADIENT_WINDSPEED: any = {
    name: 'windSpeedGradient',
    belowColor: WIND_CALM,
    // minValue is >=, maxValue is <
    stops: [
        {minValue: 5, maxValue: 30, minColor: WIND_CALM, maxColor: WIND_STOP1},
        {minValue: 30, maxValue: 50, minColor: WIND_STOP1, maxColor: WIND_MAX}
    ],
    aboveColor: WIND_MAX
};

// Attributes associated with mouse hover tracking
export const TRACKING: any = {
    line: {
        stroke: '#0000ff',
        'stroke-width': '1px'
    },
    textBack: {
        fill: '#ffffff',
        opacity: 0.7
    },
    text: {
        fill: '#0000ff',
        'font-family': 'helvetica',
        'font-size': '12px'
    },
};

export const CURRENT_TIME_LINE = {
    attrs: [
        {name: 'stroke', value: '#606060'},
        {name: 'stroke-width', value: 1}
    ],
    styles: [
        {name: 'stroke-dasharray', value: ('5, 5')}
    ]
};

export const HORIZ_GRID: any = {
    major: {
        stroke: '#202020',
        'stroke-width': '1px'
    },
    minor: {
        stroke: '#808080',
        'stroke-width': '0.5px'
    },
    freezing: {
        stroke: '#00a2ff',
        'stroke-width': '1px'
    },
    // TODO: move this out of HORIZ since its a vertical line
    midnight: {
        stroke: '#606060',
        'stroke-width': '1px'
    }
};
