import { Component, HostListener, OnInit } from '@angular/core';
import { NwsApiService } from '../services/nws-api.service';
import * as moment from 'moment';
import * as suncalc from 'suncalc';
import debounce from 'lodash/debounce';
import forEach from 'lodash/forEach';
import map from 'lodash/map';
import minBy from 'lodash/minBy';
import min from 'lodash/min';
import maxBy from 'lodash/maxBy';
import max from 'lodash/max';
import find from 'lodash/find';
import * as d3 from 'd3';
import { isNgTemplate } from '@angular/compiler';
import { SvgParts } from './nws.svg.util';
import { GRADIENT_DEWPOINT, GRADIENT_SKYCOVER, GRADIENT_WINDCHILL, GRADIENT_WINDSPEED, HORIZ_GRID, LIGHT, NWS_ATTR } from './nws.attributes';

const ONE_HOUR = 3600000;
const ONE_DAY = ONE_HOUR * 24;
const TOP_GAP = 20;
const CHART_GAP = 20;
const BOTTOM_GAP = 30;

@Component({
  selector: 'app-nws',
  templateUrl: './nws.component.html',
  styleUrls: ['./nws.component.less']
})
export class NwsComponent implements OnInit {

    // note: '42.7,-78.75' is North Boston, NY
    lat: string = '42.7';
    lon: string = '-78.75';
    gridLocation: string = '';
    nwsSvgWidth: number = 900;
    nwsSvgHeight: number = 530;

    // The main plottable forcast data
    forcastData;

    now: moment.Moment;
    hoverDate;

    radarSiteId = 'BUF';
    radarImageId = 'N0R';
    radarTopoLong;
    radarReflect;
    radarOverlays: string[] = [];

    radarCountyLong;
    radarHighwaysLong;

    /* ********** SVG Stuff ********** */
    svgMargin: any = {
        top: 20,
        right: 50,
        bottom: 40,
        left: 50
    };
    svgWidth: number;
    svgHeight: number;
    nextChartYposition: number;
    dynamicChartHeight: number;

    // The class we use for the low level helpers methods
    svgParts;
    // Current Scale values. May not need if we move all drawing stuff into svgParts
    xScale: any;

    // SVG group used to hold the elements that move with (including) the time line
    svgTrackingGroup: any;

    constructor(private nwsApiService: NwsApiService) {
    }

    resizeDebounced = debounce((eventTarget) => {
        this.nwsSvgWidth = eventTarget.innerWidth - 60;
        this.nwsSvgHeight = eventTarget.innerHeight - 300;
        console.log(`Resize width:${this.nwsSvgWidth}, ${this.nwsSvgHeight}`);
        this.draw();
    }, 200);

    ngOnInit() {
        this.now = moment();
        console.log('Current time is:', this.now.format('hh:mm:ssa'));
        console.log(`SVG working area - width ${this.svgWidth}, height:${this.svgHeight}`);

        this.svgParts = new SvgParts();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.resizeDebounced(event.target);
    }

    draw() {
        this.svgWidth = this.nwsSvgWidth - this.svgMargin.left - this.svgMargin.right;
        this.svgHeight = this.nwsSvgHeight - this.svgMargin.top - this.svgMargin.bottom;    
        this.nextChartYposition = TOP_GAP;
        const numCharts = 3;
        const availableChartHeight = this.svgHeight - TOP_GAP - BOTTOM_GAP - (CHART_GAP * (numCharts-1));
        this.dynamicChartHeight = availableChartHeight / numCharts;
        this.svgParts.reset();
        this.plotNwsData(this.forcastData);
    }

    async onClickNws(lat: string, lon: string) {
        console.log(`Click test at lat:${lat} lon:${lon}`);

        this.forcastData = await this.nwsApiService.getForcastData(lat, lon);

        const getLocationMetaData = this.nwsApiService.getLocationMetaData();
        this.gridLocation = `${getLocationMetaData.relativeLocation.properties.city}, ${getLocationMetaData.relativeLocation.properties.state}`;
        this.draw();
    }

    onClickRadar(siteId: string = 'BUF', imageId: string = 'N0R') {
        // The complete list:
        // const ridgeOverlays = ['County', 'Rivers', 'Highways', ['Cities', 'City']];
        const ridgeOverlays = ['County', 'Highways'];


        // This only ever needs to be done one
        this.radarTopoLong = this.nwsApiService.radarRidgeOverlays('Topo', siteId);
        // This will update
        this.radarReflect = this.nwsApiService.radar(siteId, imageId);
        // This only ever needs to be done one
        forEach(ridgeOverlays, (overlay) => {
            this.radarOverlays.push(this.nwsApiService.radarRidgeOverlays(overlay, siteId));
        });
    }

    onClickTest2() {
        const simulatedData = [
            {tempF: 60, dewpointF: 48, windChillF: null, heatIndexF: null, skyCoverPct: 0,   probPrecipPct: 0,   windSpeedMph: 20, windGustMph: 20, qp: null, qh: 0, ceilingHeightFt: null,  unixTime: 1567695600000, localTime: '9/05 11am'},
            {tempF: 50, dewpointF: 49, windChillF: null, heatIndexF: null, skyCoverPct: 8,   probPrecipPct: 0,   windSpeedMph: 15, windGustMph: 20, qp: null, qh: 0, ceilingHeightFt: null,  unixTime: 1567699200000, localTime: '9/05 12pm'},
            {tempF: 56, dewpointF: 59, windChillF: null, heatIndexF: null, skyCoverPct: 25,  probPrecipPct: 5,   windSpeedMph: 25, windGustMph: 28, qp: null, qh: 0, ceilingHeightFt: null,  unixTime: 1567702800000, localTime: '9/05 1pm'},
            {tempF: 64, dewpointF: 62, windChillF: null, heatIndexF: null, skyCoverPct: 44,  probPrecipPct: 5,   windSpeedMph: 22, windGustMph: 24, qp: null, qh: 0, ceilingHeightFt: null,  unixTime: 1567706400000, localTime: '9/05 2pm'},
            {tempF: 67, dewpointF: 65, windChillF: null, heatIndexF: null, skyCoverPct: 60,  probPrecipPct: 8,   windSpeedMph: 18, windGustMph: 20, qp: 0.2,  qh: 2, ceilingHeightFt: null,  unixTime: 1567710000000, localTime: '9/05 3pm'},
            {tempF: 72, dewpointF: 69, windChillF: null, heatIndexF: null, skyCoverPct: 60,  probPrecipPct: 17,  windSpeedMph: 12, windGustMph: 20, qp: null, qh: 0, ceilingHeightFt: null,  unixTime: 1567713600000, localTime: '9/05 4pm'},
            {tempF: 78, dewpointF: 71, windChillF: null, heatIndexF: 79  , skyCoverPct: 90,  probPrecipPct: 50,  windSpeedMph: 7,  windGustMph: 10, qp: 0.4,  qh: 4, ceilingHeightFt: 25000, unixTime: 1567717200000, localTime: '9/05 5pm'},
            {tempF: 76, dewpointF: 70, windChillF: null, heatIndexF: 81,   skyCoverPct: 100, probPrecipPct: 80,  windSpeedMph: 0,  windGustMph: 5,  qp: null, qh: 0, ceilingHeightFt: 25000, unixTime: 1567720800000, localTime: '9/05 6pm'},
            {tempF: 71, dewpointF: 69, windChillF: null, heatIndexF: 81,   skyCoverPct: 100, probPrecipPct: 100, windSpeedMph: 2,  windGustMph: 5,  qp: null, qh: 0, ceilingHeightFt: 12000, unixTime: 1567724400000, localTime: '9/05 7pm'},
            {tempF: 70, dewpointF: 68, windChillF: null, heatIndexF: null, skyCoverPct: 100, probPrecipPct: 95,  windSpeedMph: 5,  windGustMph: 5,  qp: null, qh: 0, ceilingHeightFt: 12000, unixTime: 1567728000000, localTime: '9/05 8pm'},
            {tempF: 62, dewpointF: 66, windChillF: null, heatIndexF: null, skyCoverPct: 95,  probPrecipPct: 80,  windSpeedMph: 5,  windGustMph: 5,  qp: 0.5,  qh: 1, ceilingHeightFt: 25000,  unixTime: 1567731600000, localTime: '9/05 9pm'},
            {tempF: 52, dewpointF: 63, windChillF: null, heatIndexF: null, skyCoverPct: 80,  probPrecipPct: 40,  windSpeedMph: 5,  windGustMph: 8,  qp: 0.3,  qh: 2, ceilingHeightFt: 25000,  unixTime: 1567735200000, localTime: '9/05 10pm'},
            {tempF: 55, dewpointF: 55, windChillF: null, heatIndexF: null, skyCoverPct: 50,  probPrecipPct: 20,  windSpeedMph: 20, windGustMph: 22, qp: null, qh: 0, ceilingHeightFt: 12000,  unixTime: 1567738800000, localTime: '9/05 11pm'},
            {tempF: 55, dewpointF: 45, windChillF: null, heatIndexF: 77,   skyCoverPct: 22,  probPrecipPct: 20,  windSpeedMph: 25, windGustMph: 35, qp: null, qh: 0, ceilingHeightFt: 12000,  unixTime: 1567742400000, localTime: '9/06 12am'},
            {tempF: 52, dewpointF: 42, windChillF: 44,   heatIndexF: 74,   skyCoverPct: 12,  probPrecipPct: 5,   windSpeedMph: 32, windGustMph: 35, qp: null, qh: 0, ceilingHeightFt: 2500,   unixTime: 1567746000000, localTime: '9/06 1am'},
            {tempF: 50, dewpointF: 40, windChillF: 38,   heatIndexF: null, skyCoverPct: 5,   probPrecipPct: 2,   windSpeedMph: 42, windGustMph: 48, qp: null, qh: 0, ceilingHeightFt: 2500,   unixTime: 1567749600000, localTime: '9/06 2am'},
            {tempF: 55, dewpointF: 40, windChillF: 32,   heatIndexF: null, skyCoverPct: 5,   probPrecipPct: 2,   windSpeedMph: 50, windGustMph: 55, qp: null, qh: 0, ceilingHeightFt: 1200,   unixTime: 1567753200000, localTime: '9/06 3am'},
            {tempF: 56, dewpointF: 40, windChillF: 25,   heatIndexF: null, skyCoverPct: 15,  probPrecipPct: 0,   windSpeedMph: 54, windGustMph: 60, qp: null, qh: 0, ceilingHeightFt: 1200,   unixTime: 1567756800000, localTime: '9/06 4am'},
            {tempF: 56, dewpointF: 40, windChillF: 5,    heatIndexF: 66,   skyCoverPct: 15,  probPrecipPct: 0,   windSpeedMph: 40, windGustMph: 48, qp: null, qh: 0, ceilingHeightFt: 250,    unixTime: 1567760400000, localTime: '9/06 5am'},
            {tempF: 56, dewpointF: 40, windChillF: -15,  heatIndexF: null, skyCoverPct: 15,  probPrecipPct: 0,   windSpeedMph: 27, windGustMph: 30, qp: null, qh: 0, ceilingHeightFt: 250,    unixTime: 1567764000000, localTime: '9/06 6am'},
            {tempF: 56, dewpointF: 35, windChillF: -25,  heatIndexF: null, skyCoverPct: 15,  probPrecipPct: 5,   windSpeedMph: 22, windGustMph: 25, qp: null, qh: 0, ceilingHeightFt: 10,    unixTime: 1567767600000, localTime: '9/06 7am'},
            {tempF: 50, dewpointF: 34, windChillF: 0,    heatIndexF: null, skyCoverPct: 15,  probPrecipPct: 5,   windSpeedMph: 15, windGustMph: 28, qp: null, qh: 0, ceilingHeightFt: 10,    unixTime: 1567771200000, localTime: '9/06 8am'},
            {tempF: 60, dewpointF: 35, windChillF: 10,   heatIndexF: null, skyCoverPct: 15,  probPrecipPct: 5,   windSpeedMph: 20, windGustMph: 25, qp: null, qh: 0, ceilingHeightFt: null,  unixTime: 1567774800000, localTime: '9/06 9am'}
        ]
        const sim2 = map(simulatedData, (entry => {
            return {
                unixTime: entry.unixTime,
                tempF: {value: entry.tempF, hours: 1},
                dewpointF: {value: entry.dewpointF, hours: 1},
                heatIndexF: {value: entry.heatIndexF, hours: 1},
                windChillF: {value: entry.windChillF, hours: 1},
                skyCoverPct: {value: entry.skyCoverPct, hours: 1},
                probPrecipPct: {value: entry.probPrecipPct, hours: 1},
                windSpeedMph: {value: entry.windSpeedMph, hours: 1},
                windGustMph: {value: entry.windGustMph, hours: 1},
                ceilingHeightFt: {value: entry.ceilingHeightFt, hours: 1},
                quantPrecipIn: {value: entry.qp, hours: entry.qh}
            }
        }));
        this.plotNwsData(sim2);
    }

    onClickTest3() {
        console.log(`Click test drawing with D3`);
        // this.d3Sample();
        this.d3Sample2();
    }

    getLightData(firstMidnight, lastMidnight, lat, lon) {
        const lightData = [];
        for (let dateMs = firstMidnight; dateMs <= lastMidnight; dateMs += ONE_DAY) {
            const date = new Date(dateMs);
            const nextDate = new Date(dateMs + ONE_DAY);
            const sunTimes = suncalc.getTimes(date, lat, lon);
            // console.log('sunTimes:', sunTimes);
            lightData.push(...[
                {light: LIGHT.night, start: date, end: sunTimes.nightEnd},
                {light: LIGHT.dawn, start: sunTimes.nightEnd, end: sunTimes.goldenHourEnd},
                {light: LIGHT.day, start: sunTimes.goldenHourEnd, end: sunTimes.goldenHour},
                {light: LIGHT.dusk, start: sunTimes.goldenHour, end: sunTimes.night},
                {light: LIGHT.night, start: sunTimes.night, end: nextDate},
            ]);
        }
        return lightData;
    }

    getDayData(firstMidnight, lastMidnight) {
        const dayData = [];
        for (let dateMs = firstMidnight; dateMs <= lastMidnight;) {
            const date = new Date(dateMs);
            const noon = new Date(dateMs + ONE_DAY / 2);
            let nextDateMs = dateMs + ONE_DAY;

            // Daylight savings time. Manually adjust by one hour.
            const todayDST = moment(date).isDST();
            const tomorrowDST = moment(nextDateMs).isDST();
            if (todayDST && !tomorrowDST) {
                nextDateMs += ONE_HOUR;
            } else if (!todayDST && tomorrowDST) {
                nextDateMs -= ONE_HOUR;
            }

            const nextDate = new Date(nextDateMs);

            dayData.push( {start: date, noon: noon, end: nextDate} );
            dateMs = nextDateMs;
        }
        return dayData;
    }

    hoverTimeChange(newDate) {
        // fake current date for sun position testing
        // newDate.setMonth(newDate.getMonth() - 3);
        this.hoverDate = newDate;
    }

    /**
     * Main function called to build the SVG content
     * @param data Raw data from API call
     */
    plotNwsData(nwsData) {
        if (!nwsData || !nwsData.length) {
            return;
        }
        const self = this;
        this.nextChartYposition = TOP_GAP;
        console.log('plotNwsData - nwsData:', nwsData);

        // Add Stuff to the data to help build the SVG
        for(let i = 0; i < nwsData.length; i++) {
            // allow the current data point access to the previous one
            nwsData[i].prev = i > 0 ? nwsData[i - 1] : null;
            // allow the current data point access to the next one
            nwsData[i].next = i < nwsData.length - 1 ? nwsData[i + 1] : null;
            // create a moment object once. Use to find day boundries
            nwsData[i].moment = moment(nwsData[i].unixTime);
        }

        const timeBounds = {
            firstMs: nwsData[0].unixTime,
            lastMs: nwsData[nwsData.length - 1].unixTime,
            firstDate: new Date(nwsData[0].unixTime),
            lastDate: new Date(nwsData[nwsData.length - 1].unixTime)
        };

        // let firstDate = nwsData[0].unixTime;
        // let lastDate = nwsData[ nwsData.length-1 ].unixTime;
        const firstMidnight = moment(timeBounds.firstMs).startOf('day').valueOf();
        const lastMidnight = moment(timeBounds.lastMs).add(23, 'hours').startOf('day').valueOf();
        const lightData = this.getLightData(firstMidnight, lastMidnight, this.lat, this.lon);
        const dayData = this.getDayData(firstMidnight, lastMidnight);

        // TODO: Put in a function
        // Dynamic tick marks
        const maxTicks = 50;
        const ticksIntervals = [1, 2, 3, 4, 6, 8, 12, 24];
        let tiIndex = 0;
        let tickHours = ticksIntervals[tiIndex];
        while (tickHours <= 24 && nwsData.length / tickHours > maxTicks) {
            tickHours = ticksIntervals[++tiIndex];
        }

        /* ********** Set up axes and scales ********** */

        // todo - select this one onInit
        const svgContainer = d3.select('svg.nws-container');
        this.svgParts.setMainSvg(svgContainer);

        // Add the daylight gradients - only needed once per data
        this.svgParts.addGradientDef(svgContainer, 'dawnGradient', [
            {offset: '0%', color: LIGHT.night, opacity: LIGHT.opacity},
            {offset: '100%', color: LIGHT.day, opacity: LIGHT.opacity}
        ]);
        this.svgParts.addGradientDef(svgContainer, 'duskGradient', [
            {offset: '0%', color: LIGHT.day, opacity: LIGHT.opacity},
            {offset: '100%', color: LIGHT.night, opacity: LIGHT.opacity}
        ]);

        svgContainer.select('.graph-container').remove();
        // The All of the SVG content, but address shifted to leave room for axes
        const svgGraph = svgContainer.append('g')
            .attr('class', 'graph-container')
            .attr('transform', function (d, i) {
                return `translate(${self.svgMargin.left}, ${self.svgMargin.top})`;
            });
        // The group where the time tracking vertical line and values go
        this.svgTrackingGroup = svgContainer.append('g')
            .attr('class', 'tracking-line-group')
            .style('display', 'none')
            .attr('transform', function (d, i) {
                return `translate(${self.svgMargin.left}, ${self.svgMargin.top})`;
            });
        // todo: svgTrackingGroup can be local
        this.svgParts.setSvgTrackingGroup(this.svgTrackingGroup);

        // Group for Graphs themselves, but also include horizontal axis for pan/zoom
        const svgDataGroup = svgGraph.append('g')
            .attr('class', 'data-group');
        // Group for Vertical axis (units). One per graph
        const svgXAxisGroup = svgGraph.append('g')
            .attr('class', 'x-axis-container');
        // Group for Horizontal axis. One top, one bottom
        const svgYAxisGroup = svgGraph.append('g')
            .attr('class', 'y-axis-container');

        // xScale should be the day/hour and common across all data.
        const xScale = d3.scaleTime()
            // domain is the min/max values of the data
            .domain([timeBounds.firstDate, timeBounds.lastDate])
            // range is the addressable pixels
            .range([0, this.svgWidth]);
        const yScale = d3.scaleLinear()
            .domain([0, 100])
            .range([self.svgHeight, 0]);

        // The svgParts class (Chart drawer) uses a common xScale
        this.svgParts.setXscale(xScale);
        this.svgParts.setYscaleFull(yScale);

        // Top / Bottom Axis Generator
        const xAxisTopGenerator = d3.axisTop()          // text above ticks
            .scale(xScale);
        const xAxisBottomGenerator = d3.axisBottom(xScale)    // text below ticks
            .ticks(d3.timeHour.every(tickHours))
            // .tickFormat(d3.timeFormat('%I %p'));
            .tickFormat(function(d, i) {
                const dateMs = d.getTime();
                const formattedDate = moment(dateMs).format('h a');
                return formattedDate;
            });

        const gTopAxis = svgXAxisGroup.append('g')
            .attr('class', 'axis-bottom')
            .call(xAxisTopGenerator)    // todo: remove this and get styles this puts on 'dayAxisBlocks'
            .attr('transform', `translate(0, 0)`);    // keep at top margin
        this.svgParts.dayAxisBlocks(gTopAxis, dayData);

        const gBottomAxis = svgXAxisGroup.append('g')
            .attr('class', 'axis-bottom')
            .call(xAxisBottomGenerator)
            .attr('transform', `translate(0, ${this.svgHeight})`);    // move it to bottom
        gBottomAxis.selectAll('text')
            .style('text-anchor', 'end')
            .attr('dx', '-.8em')
            .attr('dy', '.15em')
            .attr('transform', 'rotate(-65)');

        /* ********** Zoom ********** */

        // let zoom = d3.zoom();
        // svgContainer.call(zoom);
        // zoom
        //     // .scaleExtent([1, 40])
        //     // .extent([[0, 0], [400, 350]])
        //     // .translateExtent([[-100, -100], [this.svgWidth + 90, this.svgHeight + 100]])
        //     // .translateExtent([[-50, -20], [1000, -20]])
        //     // .translateExtent([[0, 0], [100, 100]])
        //     .on("zoom", zoomed);

        // function zoomed() {
        //     console.log('zoomed - d3.event.transform:', d3.event.transform);
        //     svgDataGroup.attr("transform", d3.event.transform);
        //     const transform = d3.event.transform;

        //     const x =  transform.x;
        //     // const y =  transform.y;
        //     const y =  0;
        //     // const k =  transform.k;
        //     const k =  1;
        //     // svgDataGroup.attr("transform", `translate(${x}, ${y}) scale(${k})`);
        //     // svgXAxisGroup.attr("transform", `translate(${x}, ${y}) scale(${k})`);
        //     // gX.call(xAxis.scale(d3.event.transform.rescaleX(x)));
        //     // gY.call(yAxis.scale(d3.event.transform.rescaleY(y)));

        //     const xNewScale = transform.rescaleX(xScale);
        //     xAxisTopGenerator.scale(xNewScale);
        //     svgXAxisGroup.call(xAxisTopGenerator);
        // }

        /* ********** Draw the Graphs ********** */

        // MouseOver vertical line
        this.svgParts.setupHover(
            svgContainer,
            this.svgTrackingGroup,
            nwsData,
            timeBounds,
            self.svgMargin.left,
            self.hoverTimeChange.bind(this)
        );

        this.svgParts.addGraphClipArea(svgDataGroup, timeBounds);
        this.svgParts.svgDaylightGroup(svgDataGroup, lightData, yScale, null);

        // Build up the graph blocks
        this.graphTemperature(nwsData, lightData, timeBounds, svgYAxisGroup, svgDataGroup);
        this.graphPercentages(nwsData, lightData, timeBounds, svgYAxisGroup, svgDataGroup);
        this.graphWind(nwsData, lightData, timeBounds, svgYAxisGroup, svgDataGroup);

        this.svgParts.svgCurrentTime(svgDataGroup, lightData, yScale, null);
    }

    /**
     * Helper function to set up vertical axis ranges and domain based on data
     * @param data Raw data from API call
     * @param propList Properties in the data to get min/max (since more than one per chart)
     * @param major Value of Major axis
     * @param minor Value of Minor axis
     */
    findDataRanges(data, propList, major, minor) {
        const minList = [];
        const maxList = [];
        const buffer = major / 10;

        forEach(propList, (prop) => {
            minList.push(minBy(data, prop+'.value')[prop].value);
            maxList.push(maxBy(data, prop+'.value')[prop].value);
        });
        const minData = min(minList);
        const maxData = max(maxList);
        // use minor for the padding
        const minDomain = Math.floor((minData - buffer) / minor) * minor;
        const maxDomain = Math.ceil((maxData + buffer) / minor) * minor;
        // start the major axis calculation on a major value
        const minAxis = Math.floor((minData - buffer) / major) * major;
        const majorList = [];
        const minorList = [];
        for (let val = minAxis; val <= maxDomain; val += major) {
            if (val > minDomain && val < maxDomain) {
                majorList.push(val);
                if (val + minor < maxDomain) {
                    minorList.push(val + minor);
                }
            }
        }
        return {
            minDomain: minDomain,
            maxDomain: maxDomain,
            majorList: majorList,
            minorList: minorList
        };
    }

    /**
     * Function used to draw the Temperature graphs
     * @param data raw data to plot
     * @param lightData object defining the times of day for daylight transitions
     * @param timeBounds object providing the times for the bounds of the data
     * @param baseAxisG base SVG g element the left axis will render under
     * @param baseDataG base SVG g element this graph will render under
     */
    graphTemperature(data, lightData, timeBounds, baseAxisG, baseDataG) {
        const chartId = 'temperatures';
        const top = this.nextChartYposition;
        const bottom = top + this.dynamicChartHeight;
        this.nextChartYposition = bottom + CHART_GAP;

        // Grab the ranges from the data
        const ranges = this.findDataRanges(data, ['tempF', 'dewpointF', 'heatIndexF', 'windChillF'], 10, 5);

        const yScale = d3.scaleLinear()
            .domain([ranges.minDomain, ranges.maxDomain])
            .range([bottom, top]);

        const chartBounds = {
            valueBottom: ranges.minDomain,
            valueTop: ranges.maxDomain,
            hAxes: [
                {values: ranges.majorList, stroke: HORIZ_GRID.minor.stroke, 'stroke-width': HORIZ_GRID.minor['stroke-width']}
            ]
        };
        if (chartBounds.valueBottom <= 32) {
            chartBounds.hAxes.push({values: [32], stroke: HORIZ_GRID.freezing.stroke, 'stroke-width': HORIZ_GRID.freezing['stroke-width']});
        }
        if (chartBounds.valueTop >= 70) {
            chartBounds.hAxes.push({values: [70], stroke: HORIZ_GRID.major.stroke, 'stroke-width': HORIZ_GRID.major['stroke-width']});
        }

        const yAxisLeftGenerator = d3.axisLeft()    // text left of ticks
            .scale(yScale);
        const yAxisRightGenerator = d3.axisRight()  // text right of ticks
            .scale(yScale);

        const gLeftAxis = baseAxisG.append('g')
            .attr('class', 'axis-left')
            .call(yAxisLeftGenerator);
        const gRightAxis = baseAxisG.append('g')
            .attr('class', 'axis-right')
            .call(yAxisRightGenerator)
            .attr('transform', `translate(${this.svgWidth}, 0)`);    // move it to right

        const chartGroup = this.svgParts.svgChartGroup(baseDataG, chartId, yScale, timeBounds, chartBounds);
        const hourGroup = this.svgParts.svgHourGroup(chartGroup, chartId, data);

        this.svgParts.svgHourFill(hourGroup, 'dewpointF', yScale, chartBounds, GRADIENT_DEWPOINT);
        this.svgParts.svgHourFill(hourGroup, 'windChillF', yScale, chartBounds, GRADIENT_WINDCHILL);

        this.svgParts.svgHorizontalGridGroup(chartGroup, timeBounds, yScale, chartBounds);
        this.svgParts.svgVerticalGrid(hourGroup, yScale, chartBounds);

        this.svgParts.svgChartLine(chartGroup, chartId, data, 'heatIndexF', yScale, {attrs: NWS_ATTR.heatIndex});
        this.svgParts.svgChartLine(chartGroup, chartId, data, 'tempF', yScale, {attrs: NWS_ATTR.temperature});
        this.svgParts.svgChartLine(chartGroup, chartId, data, 'dewpointF', yScale, {attrs: NWS_ATTR.dewpoint});
        this.svgParts.svgChartLine(chartGroup, chartId, data, 'windChillF', yScale, {attrs: NWS_ATTR.windChill});
    }

    /**
     * Function used to draw the percentages graph
     * @param data raw data to plot
     * @param lightData object defining the times of day for daylight transitions
     * @param timeBounds object providing the times for the bounds of the data
     * @param baseAxisG base SVG g element the left axis will render under
     * @param baseDataG base SVG g element this graph will render under
     */
    graphPercentages(data, lightData, timeBounds, baseAxisG, baseDataG) {
        const chartId = 'percentages';
        const top = this.nextChartYposition;
        const bottom = top + this.dynamicChartHeight;
        this.nextChartYposition = bottom + CHART_GAP;

        const quantPrecipRanges = this.findDataRanges(data, ['quantPrecipIn'], 0.4, 0.2);
        if (quantPrecipRanges.minDomain < 0) {
            quantPrecipRanges.minDomain = 0;
        }
        const yScaleQuantPrecip = d3.scaleLinear()
            .domain([quantPrecipRanges.minDomain, quantPrecipRanges.maxDomain])
            .range([bottom, top]);
        const yScaleCeiling = d3.scaleLog()
            .domain([10, 25000])
            .range([bottom, top]);

        const yScale = d3.scaleLinear()
            .domain([0, 100]) // Make dynamic based on data range.
            .range([bottom, top]);

        const chartBounds = {
            valueBottom: 0,
            valueTop: 100,
            hAxes: [
                {values: [50, 100], stroke: HORIZ_GRID.major.stroke, 'stroke-width': HORIZ_GRID.major['stroke-width']},
                {values: [25, 75], stroke: HORIZ_GRID.minor.stroke, 'stroke-width': HORIZ_GRID.minor['stroke-width']},
            ]
        };

        const yAxisLeftGenerator = d3.axisLeft()    // text left of ticks
            .scale(yScale);
        // const yAxisRightGenerator = d3.axisRight()  // text right of ticks
        //     .scale(yScale);
        const yAxisRightGenerator = d3.axisRight()  // text right of ticks
            .scale(yScaleQuantPrecip);

        const gLeftAxis = baseAxisG.append('g')
            .attr('class', 'axis-left')
            .call(yAxisLeftGenerator);
        const gRightAxis = baseAxisG.append('g')
            .attr('class', 'axis-right')
            .call(yAxisRightGenerator)
            .attr('transform', `translate(${this.svgWidth}, 0)`);    // move it to right

        const chartGroup = this.svgParts.svgChartGroup(baseDataG, chartId, yScale, timeBounds, chartBounds);
        // works, but moving outside of charts
        // this.svgParts.svgDaylightGroup(chartGroup, lightData, yScale, chartBounds);
        const hourGroup = this.svgParts.svgHourGroup(chartGroup, chartId, data);

        this.svgParts.svgHourFill(hourGroup, 'skyCoverPct', yScale, chartBounds, GRADIENT_SKYCOVER);

        this.svgParts.svgChartLine(chartGroup, chartId, data, 'probPrecipPct', yScale, {attrs: NWS_ATTR.probPrecip}, chartBounds);
        const hourGroup2 = this.svgParts.svgHourGroup(chartGroup, chartId+'_super', data);
        this.svgParts.svgMultiHourRect(hourGroup2, 'quantPrecipIn', yScaleQuantPrecip, {attrs: NWS_ATTR.quantPrecip});
        this.svgParts.svgMultiHourRect(hourGroup2, 'snowfallIn', yScaleQuantPrecip, {attrs: NWS_ATTR.snowfall});

        this.svgParts.svgHorizontalGridGroup(chartGroup, timeBounds, yScale, chartBounds);
        this.svgParts.svgVerticalGrid(hourGroup, yScale, chartBounds);

        this.svgParts.svgChartLine(chartGroup, chartId, data, 'humidityPct', yScale, {attrs: NWS_ATTR.relativeHumidity});
        this.svgParts.svgChartLine(chartGroup, chartId, data, 'ceilingHeightFt', yScaleCeiling, {attrs: NWS_ATTR.ceilingHeight});
        this.svgParts.svgChartLine(chartGroup, chartId, data, 'skyCoverPct', yScale, {attrs: NWS_ATTR.skyCover});
    }

    graphWind(data, lightData, timeBounds, baseAxisG, baseDataG) {
        const chartId = 'wind';
        const top = this.nextChartYposition;
        const bottom = top + this.dynamicChartHeight;
        this.nextChartYposition = bottom + CHART_GAP;

        const ranges = this.findDataRanges(data, ['windSpeedMph', 'windGustMph'], 10, 5);
        // Override is max speed is small
        if (ranges.maxDomain < 40) {
            ranges.minDomain = 0;
            ranges.maxDomain = 40;
            ranges.majorList = [0, 10, 20, 30, 40];
            ranges.minorList = [];
        }
        else if (ranges.minDomain < 0) {
            ranges.minDomain = 0;
        }

        const yScale = d3.scaleLinear()
            .domain([ranges.minDomain, ranges.maxDomain])
            .range([bottom, top]);

        const chartBounds = {
            valueBottom: ranges.minDomain,
            valueTop: ranges.maxDomain,
            hAxes: [
                {values: ranges.majorList, stroke: HORIZ_GRID.minor.stroke, 'stroke-width': HORIZ_GRID.minor['stroke-width']}
            ]
        };

        const yAxisLeftGenerator = d3.axisLeft()    // text left of ticks
            .scale(yScale);
        const yAxisRightGenerator = d3.axisRight()  // text right of ticks
            .scale(yScale);

        const gLeftAxis = baseAxisG.append('g')
            .attr('class', 'axis-left')
            .call(yAxisLeftGenerator);
        const gRightAxis = baseAxisG.append('g')
            .attr('class', 'axis-right')
            .call(yAxisRightGenerator)
            .attr('transform', `translate(${this.svgWidth}, 0)`);    // move it to right

        const chartGroup = this.svgParts.svgChartGroup(baseDataG, chartId, yScale, timeBounds, chartBounds);
        const hourGroup = this.svgParts.svgHourGroup(chartGroup, chartId, data);

        this.svgParts.svgHourFill(hourGroup, 'windSpeedMph', yScale, chartBounds, GRADIENT_WINDSPEED);

        this.svgParts.svgHorizontalGridGroup(chartGroup, timeBounds, yScale, chartBounds);
        this.svgParts.svgVerticalGrid(hourGroup, yScale, chartBounds);

        this.svgParts.svgChartLine(chartGroup, chartId, data, 'windGustMph', yScale, {attrs: NWS_ATTR.windGust});
        this.svgParts.svgChartLine(chartGroup, chartId, data, 'windSpeedMph', yScale, {attrs: NWS_ATTR.windSpeed});

        const hourGroup2 = this.svgParts.svgHourGroup(chartGroup, chartId+'_super', data);
        this.svgParts.svgHourArrow(hourGroup2, chartId, 'windDirection', 'windSpeedMph', yScale, {attrs: NWS_ATTR.windDirection});
    }

    /* ********************************************** */
    /* ********** Reference and test stuff ********** */
    /* ********************************************** */

    d3Sample() {
        console.log('D3 Version:', d3.version);

        const eleSVG = d3.select('svg');
        console.log('eleSVG:', eleSVG);

        eleSVG
            .append('circle')
            .attr('cx', 20)
            .attr('cy', 30)
            .attr('r', 10);

        eleSVG
            .append('line')
            .attr('x1', 105)
            .attr('y1', 5)
            .attr('x2', 150)
            .attr('y2', 50)
            .style('stroke','red')
            .style('stroke-width','4px');
    }

    d3Sample2() {
        var data = [100, 400, 300, 900, 850, 1000]

        var width = 500,
            barHeight = 20,
            margin = 1;

        const eleSVG = d3.select('svg');



        var svg = d3.select("body").append("svg")
        .attr("width", 500)
        .attr("height", 300);

        // start gradients
        var defs = svg.append("defs");

        var gradient = defs.append("linearGradient")
        .attr("id", "svgGradient")
        .attr("x1", "0%")
        .attr("x2", "100%")
        // .attr("x1", "-50%")  // 1st half of red-yellow is missing
        // .attr("x2", "150%")  // 2nd half of yellow-blue is missing
        // .attr("x1", "20%")  // gradient starts here (initial color shown for 0-20%)
        // .attr("x2", "50%")  // gradient ends here (last color shown last half)
        .attr("y1", "100%")
        .attr("y2", "100%");

        gradient.append("stop")
        // .attr('class', 'start')
        .attr("offset", "0%")
        .attr("stop-color", "red")
        .attr("stop-opacity", 1);

        gradient.append("stop")
        // .attr('class', 'start')
        .attr("offset", "50%")
        .attr("stop-color", "yellow")
        .attr("stop-opacity", 1);

        gradient.append("stop")
        // .attr('class', 'end')
        .attr("offset", "100%")
        .attr("stop-color", "blue")
        .attr("stop-opacity", 1);
        // end gradients

        var xScale = d3.scaleLinear()
            // domain is the min/max values of the data
            .domain([0, d3.max(data)])
            // range is the addressable pixels
            .range([0, width]);
        const yScale = d3.scaleLinear()
            .domain([0, 500])
            .range([500, 0]);

        // Polygon
        eleSVG.append("polygon")
            .attr("points", "200,10 250,190 160,210")
            .attr("fill", "green")
            .attr("stroke", "black")
            .attr("stroke-width", 1)
        eleSVG.append("polygon")
            .attr("points", "200,10 250,190 160,210")
            .attr("fill", "#e0e0ff")
            .attr("stroke", "#6060ff")
            .attr("stroke-width", 1)
            // Shifts it down 100 pixels
            .attr("transform", function (d, i) {
                return "translate(0, 100)";
            });

            

        // This will be in markup, one for each array item:
        // <g>
        //    <rect>
        //    <text>
        var g = eleSVG.selectAll("g")
                    .data(data)
                    .enter()
                    .append("g")
                    // move the y position down for the next bar
                    .attr("transform", function (d, i) {
                        const yTrans = i * barHeight;
                        return "translate(0," + yTrans + ")";
                    });
        g.append("rect")
            .style('stroke','#808080')
            // .style('fill','#e0e0e0')
            .attr("fill", "url(#svgGradient)")
            .attr("width", function (d) {
                return xScale(d);
            })
            .attr("height", barHeight - margin);
        g.append("text")
            .attr("x", function (d) { return (xScale(d)); })
            // .attr("y", function (d) { return (yScale(d)); })
            .attr("y", barHeight / 2)
            .attr("dy", ".35em")
            .text(function (d) { return d; });


        // Decreasing line
        const fakeIncrease = [
            {h:0, v:0}, {h:10, v:10}, {h:50, v:50}, {h:100, v:100}
        ]
        const fakeFunction = d3.line()
            .x(function (d) { return d.h; })
            .y(function (d) { return yScale(d.v); });
            // .y(function (d) { return d.v; });
        const fakeLine = eleSVG.append("path")
            .attr("d", fakeFunction(fakeIncrease))
            .attr("stroke", "black")
            .attr("stroke-width", 1)
            .attr("fill", "none");

        // const lineFunction = d3.line()
        //     .x(function (d) { return d.x; })
        //     .y(function (d) { return d.y; });

        // eleSVG.append("path")
        //     .attr("d", lineFunction(tempData))
        //     .attr("stroke", "blue")
        //     .attr("stroke-width", 2)
        //     .attr("fill", "none");

    }

}
