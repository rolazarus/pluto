import { Component, Directive, Input, OnChanges, DoCheck, OnInit } from '@angular/core';

@Directive({
  selector: '[nwsCharts]'
})
export class NwsChartsDirective implements OnInit, DoCheck {
    @Input() nwsCharts: any;

    ngOnInit() {

        console.log('RDO: nwsCharts:', this.nwsCharts);

    }
    
    // ngOnChanges(changes: SimpleChanges){
    ngOnChanges(changes: any) {
        console.log('RDO: NwsChartsDirective input changed. changes:', changes);
        // if(changes.input){
        //   console.log('input changed');
        // }
        this.redraw();
    }
    ngDoCheck() {
        console.log('RDO: NwsChartsDirective doCheck:');
        // this.redraw();
    }

    redraw() {
        const self = this;

        if (this.nwsCharts[0].baseG) {
            this.nwsCharts[0].baseG.append('circle')
                .attr('stroke','lightgreen')
                .attr('stroke-width','2px')
                .attr('fill','#cccccc')
                .attr('cx', 0)
                .attr('cy', self.nwsCharts[0].yPos)
                .attr('r', 100);
        }
        if (this.nwsCharts[1].baseG) {
            this.nwsCharts[1].baseG.append('rect')
                .attr('stroke','lightblue')
                .attr('stroke-width','2px')
                .attr('fill','#cccccc')
                .attr('x', 0)
                .attr('y', self.nwsCharts[1].yPos)
                .attr('width', self.nwsCharts[1].width)
                .attr('height', 100);
        }
    }

}
