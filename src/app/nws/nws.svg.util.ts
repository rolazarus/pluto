import * as d3 from 'd3';
import * as moment from 'moment';
import find from 'lodash/find';
import forEach from 'lodash/forEach';
import map from 'lodash/map';
import sortBy from 'lodash/sortBy';
import { CURRENT_TIME_LINE, DAY_AXIS, HORIZ_GRID, TRACKING } from './nws.attributes';
// import { find } from 'rxjs/operators';

const ONE_HOUR = 3600000;

export class SvgParts {
    constructor() {
    }

    xScale;
    yScaleFull;
    eleSvg;     // top level SVG element
    svgTrackingGroup;

    metricTrackFunctions = [];  // old
    metricTracking = {};

    /**
     * Reset class
     */
    reset() {
        this.metricTrackFunctions = [];
        this.metricTracking = {};
    }

    /**
     * Store the data area x-scale in the class
     * @param scale X-Scale (time) for the graph-able data time range
     */
    setXscale(scale) {
        this.xScale = scale;
    }

    /**
     * Store the (multiple chart) area y-scale in the class
     * @param scale Y-Scale for the entire data area of the graph
     */
    setYscaleFull(yScale) {
        this.yScaleFull = yScale;
    }

    /**
     * Store the outer container SVG element in the class
     * @param element The outermost SVG element
     */
    setMainSvg(element) {
        this.eleSvg = element;
    }

    /**
     * Store SVG <g> element used for hover tracking
     * @param element The <g>> element
     */
    setSvgTrackingGroup(element) {
        this.svgTrackingGroup = element;
    }

    /**
     * Get a color in between 2 colors
     * @param color1 The 1st color ('#ffffff' for example)
     * @param color2 The 2nd color
     * @param factor How far between (ex: .5 is middle, .2 is close to color1)
     * @returns The interpolated color
     */
    interpolateColor(color1, color2, factor) {
        const r1 = parseInt(color1.slice(1, 3), 16);
        const g1 = parseInt(color1.slice(3, 5), 16);
        const b1 = parseInt(color1.slice(5, 7), 16);
        const r2 = parseInt(color2.slice(1, 3), 16);
        const g2 = parseInt(color2.slice(3, 5), 16);
        const b2 = parseInt(color2.slice(5, 7), 16);

        const r = ((r1 * (1 - factor)) + (r2 * factor));
        const g = ((g1 * (1 - factor)) + (g2 * factor));
        const b = ((b1 * (1 - factor)) + (b2 * factor));

        let rr = Math.round(r).toString(16);
        let gg = Math.round(g).toString(16);
        let bb = Math.round(b).toString(16);

        rr = rr.length < 2 ? '0' + rr : rr;
        gg = gg.length < 2 ? '0' + gg : gg;
        bb = bb.length < 2 ? '0' + bb : bb;

        return '#' + rr + gg + bb;
    }

    /**
     * Adds a linear gradient to the DOM
     * @param eleSvg Base SVG element to attach to
     * @param id The ID of the HTML DEF element created
     * @param stops[] An object defining the gradient stops
     * [
     *      {offset: '0%', color: 'white', opacity: 1}
     *      {offset: '100%', color: 'red', opacity: 1}
     * ]
     */
    addGradientDef(eleSvg, id, stops) {
        let defs = eleSvg.select(`defs.gradients`);
        if (!defs.size()) {
            defs = eleSvg.append('defs')
                .attr('class', 'gradients');
        }
        defs.select(`#${id}`).remove();

        const gradient = defs.append('linearGradient')
            .attr('id', id)
            .attr('x1', '0%')
            .attr('x2', '100%')
            .attr('y1', '100%')
            .attr('y2', '100%');

        forEach(stops, (stop) => {
            gradient.append('stop')
            .attr('offset', stop.offset)
            .attr('stop-color', stop.color)
            .attr('stop-opacity', stop.opacity);
        });

        return gradient;
    }

    addGraphClipArea(eleSvg, timeBounds) {
        const self = this;

        const y0 = self.yScaleFull(0);
        const y1 = self.yScaleFull(120);    // allow area above graph (day blocks)
        const x0 = self.xScale(timeBounds.firstDate);
        const x1 = self.xScale(timeBounds.lastDate);
        const polyString = `${x0},${y0} ${x0},${y1} ${x1},${y1} ${x1},${y0}`;

        const graphClipArea = eleSvg.append('clipPath')
            .attr('id', 'graph-clip')
            .append('polygon')
            .attr('points', polyString);

        return graphClipArea;
    }

    /**
     * Adds tracking information for a metric. Used to show metric at the line
     * @param chartId 
     * @param prop 
     * @param yScale 
     * @param options 
     */
    addMetricTracking(chartId, prop, yScale, options) {
        const BLOCK_HEIGHT = 14;
        const SHORT_BLOCK_WIDTH = 28;
        const LONG_BLOCK_WIDTH = 40;

        let blockWidth = options.attrs.text.short || SHORT_BLOCK_WIDTH;
        if (options.attrs.text.units) {
            blockWidth = options.attrs.text.long || LONG_BLOCK_WIDTH;
        }

        // note: all important "x" and "y" attributes are set on move
        this.svgTrackingGroup.append('rect')
            .attr('class', `hover-${prop}`)
            .attr('fill', TRACKING.textBack.fill)           
            .attr('opacity', TRACKING.textBack.opacity)
            .attr('rx', 5)
            .attr('ry', 5)
            .attr('width', blockWidth)
            .attr('height', BLOCK_HEIGHT);
        this.svgTrackingGroup.append('text')
            .attr('class', `hover-${prop}`)
            .attr('font-family', TRACKING.text['font-family'])
            .attr('font-size', TRACKING.text['font-size'])
            .attr('fill', options.attrs.text.fill)           
            .text('');

        if (!this.metricTracking[chartId]) {
            this.metricTracking[chartId] = [];
        }
        this.metricTracking[chartId].push({
            prop: prop,
            yScale: yScale,
            options: options
        });
    }
    
    /**
     * Render all of the metic value blocks to the right of the current hover time line
     * @param chartId 
     * @param prop 
     */
    renderTrackingValues(slot, scaledX) {
        const FROM_LINE = 3;
        const BLOCK_HEIGHT = 14;

        // Do each chart independently so we can do the orderring chart specific
        forEach(this.metricTracking, (chart) => {
            let bottomY = -10000;

            // orderred chart metric - so the top line is the top block. 2nd line is 2nd block, etc
            let ocm = map(chart, (metric) => {
                const val = slot[metric.prop].value;
                return {
                    prop: metric.prop,
                    val: val,
                    scaledY: metric.yScale(val),
                    options: metric.options
                }
            });
            ocm = sortBy(ocm, ['scaledY']);

            // For each line (orderred chart metric) in the chart
            forEach(ocm, (metric) => {
                const hoverRect = this.svgTrackingGroup.select(`rect.hover-${metric.prop}`);
                const hoverText = this.svgTrackingGroup.select(`text.hover-${metric.prop}`);

                let val = metric.val;

                if (val === null) {
                    hoverRect.style('display', 'none');
                    hoverText.style('display', 'none');
                    return;
                }

                val = Math.round(val);
                let yTop = metric.scaledY - BLOCK_HEIGHT;    // subtract to move up
                if (yTop < bottomY) {
                    yTop = bottomY;
                }
                bottomY = yTop + BLOCK_HEIGHT;

                // append units to display value if defined in the attributes
                const units = metric.options.attrs.text.units || '';
                val += units;

                hoverRect
                    .style('display', null)
                    .attr('x', scaledX + 1)
                    .attr('y', yTop);
                hoverText
                    .style('display', null)
                    .attr('x', scaledX + FROM_LINE)
                    .attr('y', yTop + 11)   // add to get to bottom of text
                    .text(val);
            });
        });
    }

    /**
     * Sets up the cursor hover that draws the time tracking vertical line
     * @param eventGroup The group element on which the events are caught
     * @param trackingGroup The group element containing the moving elements
     * @param timeBounds Object containing times bounding the graph data
     * @param xOff x-offset to start of graph (left margin) in pixels
     * @param onChange function to call when position changes
     */
    setupHover(eventGroup, trackingGroup, data, timeBounds, xOff, onChange) {
        const self = this;
        const FROM_LINE = 3;

        eventGroup
            .on('mouseover', () => trackingGroup.style('display', null))
            .on('mouseout', () => trackingGroup.style('display', 'none'))
            .on('mousemove', mousemove);

        trackingGroup.append('line')
            .attr('class', 'hover-date')
            .attr('x1', self.xScale(timeBounds.firstDate))
            .attr('y1', self.yScaleFull(0))     // % from bottom
            .attr('x2', self.xScale(timeBounds.firstDate))
            .attr('y2', self.yScaleFull(100))   // % from top
            .style('stroke', TRACKING.line.stroke)
            .style('stroke-width', TRACKING.line['stroke-width']);
        trackingGroup.append('rect')
            .attr('class', 'hover-time')
            .attr('fill', TRACKING.textBack.fill)           
            .attr('opacity', TRACKING.textBack.opacity)
            .attr('x', self.xScale(timeBounds.firstDate))
            .attr('y', self.yScaleFull(0) - 20)
            .attr('rx', 5)
            .attr('ry', 5)
            .attr('width', 50)
            .attr('height', 18);
        trackingGroup.append('text')
            .attr('class', 'hover-time')
            .attr('x', self.xScale(timeBounds.firstDate))
            .attr('y', self.yScaleFull(0) - 6)
            // .attr('textLength', 45)  // squishes/expands text to fit
            .attr('font-family', TRACKING.text['font-family'])
            .attr('font-size', TRACKING.text['font-size'])
            .attr('fill', TRACKING.text.fill)           
            .text('');

        function mousemove() {
            const position = d3.mouse(this);
            const xPos = position[0] - xOff;       // [0] = x position
            const date = self.xScale.invert(xPos);
            const dateMs = date.getTime();
            // const formattedDate = moment(dateMs).format('ddd, MMM D - h:mma');
            const formattedDate = moment(dateMs).format('h:mma');

            // console.log('svgHover - mousemove - position, date:', position, date);

            trackingGroup.select('line.hover-date')
                .attr('x1', self.xScale(date))
                .attr('x2', self.xScale(date));
            trackingGroup.select('rect.hover-time')
                .attr('x', self.xScale(date) + 1);
            trackingGroup.select('text.hover-time')
                .attr('x', self.xScale(date) + FROM_LINE)
                .text(formattedDate);

            // inform caller (on main view)
            onChange(date);

            // Update the metric values tracked under this hour.
            const slot = find(data, (hour) => {
                return dateMs < hour.unixTime + ONE_HOUR;
            });
            if (slot) {
                const xScaled = self.xScale(date);
                self.renderTrackingValues(slot, xScaled);
            }
        }
    }

    /**
     * Builds a group for all of the data in a sub-graph (chart)
     * @param baseG base SVG g element this graph will render under
     */
    svgChartGroup(baseG, id, yScale, timeBounds, chartBounds) {
        const self = this;

        const chartGroup = baseG.append('g')
            .attr('id', id)
            .attr('class', 'chart-group');

        // border around the chart
        const x0 = self.xScale(timeBounds.firstDate);
        const x1 = self.xScale(timeBounds.lastDate);
        const y0 = yScale(chartBounds.valueTop);
        const y1 = yScale(chartBounds.valueBottom);
        // Border around chart
        chartGroup.append('rect')
            .attr('x', x0)
            .attr('y', y0)
            .attr('width', x1 - x0)
            .attr('height', y1 - y0)
            .attr('fill', 'none')
            .attr('stroke', 'black')
            .attr('stroke-width', 1);

        return chartGroup;
    }

    dayAxisBlocks(baseG, lightData) {
        const self = this;
        const dayAxisGroup = baseG.append('g')
            .attr('id', 'day-axis')
            .attr('class', 'day-axis-group');

        const dayBlockGroup = dayAxisGroup.selectAll('#day-axis')
            .data(lightData)
            .enter()
            .append('g')
            .attr('class', 'day-block-group');

        const scaledY0 = self.yScaleFull(100);
        const scaledY1 = self.yScaleFull(120);

        dayBlockGroup.append('polygon')
            .attr('clip-path', 'url(#graph-clip)')
            .attr('points', function (d, i) {
                const x0 = Math.round(self.xScale(d.start));
                const y0 = scaledY0;
                const x1 = Math.round(self.xScale(d.end));
                const y1 = scaledY1;
                const polyString = `${x0},${y0} ${x0},${y1} ${x1},${y1} ${x1},${y0}`;
                // console.log('daylightGroup - polygon - polyString:', polyString);
                return polyString;
            })
            .attr('stroke', DAY_AXIS.block.stroke)
            .attr('stroke-width', DAY_AXIS.block['stroke-width'])
            .attr('fill', function (d, i) {
                const doy = d.noon.getDay();
                if (doy === 6 || doy === 0) {
                    return DAY_AXIS.block.fillWeekend;
                } 
                return DAY_AXIS.block.fillWeekday;
            });
        dayBlockGroup.append('text')
            .attr('clip-path', 'url(#graph-clip)')
            .attr('class', 'day-axis-text')
            .attr('x', function (d, i) {
                return 0 + self.xScale(d.noon);
            })
            .attr('y', 0)
            .attr('dy', '-.75em')
            .style('fill', '#202020')
            .text(function(d, i) {
                const dateMs = d.start.getTime();
                const formattedDate = moment(dateMs).format('ddd, M/D');
                return formattedDate;
            });

    }

    /**
     * Builds a group and elements to show daylight background
     * @param baseG base SVG g element this graph will render under
     * @param lightData data used to render light gradients
     * @param timeBounds Object containing times bounding the graph data
     * @param yScale y-scale (representing data) for this chart
     * @param chartBounds Object holding data bounds for this chart, and horizontal grid positions
     */
    svgDaylightGroup(baseG, lightData, yScale, chartBounds) {
        const self = this;

        const daylightGroup = baseG.append('g')
            .attr('id', 'daylight-background')
            .attr('class', 'daylight-group');

        let scaledY0;
        let scaledY1;

        if (chartBounds) {
            // Bottom and top of this chart
            scaledY0 = yScale(chartBounds.valueBottom);
            scaledY1 = yScale(chartBounds.valueTop);
        } else {
            scaledY0 = self.yScaleFull(0);
            scaledY1 = self.yScaleFull(100);
        }

        // Daylight background
        const daylightHoursGroup = daylightGroup.selectAll('#daylight-background')
        // const daylightGroup = baseG.selectAll('g.chart-group')
            .data(lightData)
            .enter()
            .append('g')
            .attr('class', 'light-segment-group');
        daylightHoursGroup.append('polygon')
            .attr('clip-path', 'url(#graph-clip)')
            .attr('points', function (d, i) {
                const x0 = Math.round(self.xScale(d.start));
                const y0 = scaledY0;
                const x1 = Math.round(self.xScale(d.end));
                const y1 = scaledY1;
                const polyString = `${x0},${y0} ${x0},${y1} ${x1},${y1} ${x1},${y0}`;
                // console.log('daylightGroup - polygon - polyString:', polyString);
                return polyString;
            })
            .attr('fill', function (d, i) {
                return d.light;
            });
    }

    svgCurrentTime (baseG) {
        const self = this;

        const path = baseG.append('path')
            .attr('d', function (d, i) {
                const date = new Date();
                const x0 = Math.round(self.xScale(date));
                const y0 = self.yScaleFull(0);
                const y1 = self.yScaleFull(100);
                const pathString = `M ${x0} ${y0} L ${x0} ${y1}`;
                return pathString;
            });
        // Add the attributes (mostly styling)
        forEach(CURRENT_TIME_LINE.attrs, (attr) => {
            path.attr(attr.name, attr.value);
        });
        forEach(CURRENT_TIME_LINE.styles, (style) => {
            path.style(style.name, style.value);
        });
    }

    /**
     * Builds a group and elements to show horizontal gridlines
     * @param baseG base SVG g element this graph will render under
     * @param timeBounds Object containing times bounding the graph data
     * @param yScale y-scale (representing data) for this chart
     * @param chartBounds Object holding data bounds for this chart, and horizontal grid positions
     */
    svgHorizontalGridGroup(chartGroup, timeBounds, yScale, chartBounds) {
        const self = this;

        const x0 = Math.round(self.xScale(timeBounds.firstDate));
        const x1 = Math.round(self.xScale(timeBounds.lastDate));

        const horizGridGroup = chartGroup.append('g')
            .attr('class', 'horiz-grid');

        forEach(chartBounds.hAxes, (axis) => {
            horizGridGroup.append('path')
            .attr('d', function (d, i) {
                let pathString = '';
                forEach(axis.values, (y) => {
                    const y1 = yScale(y);
                    const y2 = yScale(y);
                    pathString += `M ${x0} ${y1} L ${x1} ${y2} `;
                });
                return pathString;
            })
            .attr('stroke', axis.stroke)
            .attr('stroke-width', axis['stroke-width']);
        });
    }

    /**
     * Builds a group and elements to show the hours (data points)
     * @param chartG base SVG g element this graph will render under
     * @param data Chart plot data
     * @param yScale y-scale (representing data) for this chart
     * @param chartBounds Object holding data bounds for this chart, and horizontal grid positions
     */
    svgHourGroup(chartG, id, data) {
        const hourGroupContainer = chartG.append('g')
            // .attr('id', id)
            .attr('class', 'hour-group-container');

        // DATA IN !! - Build the group for each hour
        const hourGroup = hourGroupContainer.selectAll(`#${id}`)
            .data(data)
            .enter()
            .append('g')
            .attr('class', 'hour-group');

        return hourGroup;
    }

    svgVerticalGrid(hourG, yScale, chartBounds) {
        const self = this;

        // Bottom and top of this chart
        const y0 = yScale(chartBounds.valueBottom);
        const y1 = yScale(chartBounds.valueTop);

        // todo: make this a line instead of a path.
        // Vertical line at midnight for day change
        hourG.append('path')
            .attr('d', function (d, i) {
                const date = new Date(d.unixTime);
                if (d.moment.hour()) {
                    return;
                }
                const x0 = self.xScale(date);
                const pathString = `M ${x0} ${y0} L ${x0} ${y1}`;
                return pathString;
            })
            .attr('stroke', HORIZ_GRID.midnight.stroke)
            .attr('stroke-width', HORIZ_GRID.midnight['stroke-width']);

        return hourG;
    }

    /**
     * Draw a rect for an hour period - ex: Quantitative Precip
     * @param baseG base SVG g element this graph will render under
     * @param prop The property in data (sent to 'svgHourGroup') used to create rect
     * @param yScale Scale used vertically to map value 
     */
    svgMultiHourRect(baseG, prop, yScale, options) {
        const self = this;
        const poly = baseG.append('polygon')
            .attr('points', function (d, i) {
                if (!d[prop].hours) {
                    return '';
                }
                const startDate = new Date(d.unixTime);
                const endDate = new Date(d.unixTime);
                endDate.setHours(endDate.getHours() + d[prop].hours);
                const x0 = Math.round(self.xScale(startDate))
                const y0 = yScale(0);
                const x1 = Math.round(self.xScale(endDate));
                const y1 = yScale(d[prop].value);
                const polyString = `${x0},${y0} ${x0},${y1} ${x1},${y1} ${x1},${y0}`;
                // console.log('svgMultiHourRect - polygon - polyString:', polyString);
                return polyString;
            });
        // Add the attributes (mostly styling)
        forEach(options.attrs.poly, (attr) => {
            poly.attr(attr.name, attr.value);
        });

        return poly;
    }

    /**
     * Fills in an hour background based on an NWS metric  ex: Dewpoint
     * @param baseG base SVG g element this graph will render under
     * @param prop The property in data (sent to 'svgHourGroup') used to create rect
     * @param yScale Scale used vertically to map value 
     * @param chartBounds Object holding data bounds for this chart
     * @param gradientInfo ID, value stops and colors to build the gradient
     */
    svgHourFill(baseG, prop, yScale, chartBounds, gradientInfo) {
        const self = this;
        const poly = baseG.append('polygon')
            .attr('points', function (d, i) {
                if (!d.next || d[prop].value === null || d.next[prop].value === null) {
                    return '';
                }
                // Round here otherwise we get the phantom vertical lines
                const x0 = Math.round(self.xScale(d.unixTime));
                const y0 = yScale(chartBounds.valueBottom);
                const x1 = Math.round(self.xScale(d.next.unixTime));
                const y1 = yScale(chartBounds.valueTop);

                const polyString = `${x0},${y0} ${x0},${y1} ${x1},${y1} ${x1},${y0}`;
                // console.log('svgHourFill - polygon - polyString:', polyString);
                return polyString;
            })
            .attr('fill', function (d, i) {
                if (!d.next) {
                    return '';
                }
                let colorL;
                let colorR;
                const valueL = d[prop].value;
                const valueR = d.next[prop].value;

                // Left color
                forEach(gradientInfo.stops, (stop) => {
                    if (valueL >= stop.minValue && valueL < stop.maxValue) {
                        colorL = self.interpolateColor(
                            stop.minColor,
                            stop.maxColor,
                            (valueL - stop.minValue) / (stop.maxValue - stop.minValue)
                        );
                    }
                });
                if (!colorL) {
                    colorL = valueL < gradientInfo.stops[0].minValue ? gradientInfo.belowColor : gradientInfo.aboveColor;
                }
                // Right color
                forEach(gradientInfo.stops, (stop) => {
                    if (valueR >= stop.minValue && valueR < stop.maxValue) {
                        colorR = self.interpolateColor(
                            stop.minColor,
                            stop.maxColor,
                            (valueR - stop.minValue) / (stop.maxValue - stop.minValue)
                        );
                    }
                });
                if (!colorR) {
                    colorR = valueR < gradientInfo.stops[0].minValue ? gradientInfo.belowColor : gradientInfo.aboveColor;
                }
                const gradientId = gradientInfo.name + i;
                self.addGradientDef(self.eleSvg, gradientId, [
                    {offset: '0%', color: colorL, opacity: 1},
                    {offset: '100%', color: colorR, opacity: 1}
                ]);

                return `url(#${gradientId})`;
            });
            // note: This works if we don't create DEFS for the background.
            // .attr('fill', function (d, i) {
            //     let color;
            //     const value = d[prop];
            //     forEach(gradientInfo.stops, (stop) => {
            //         if (value >= stop.minValue && value < stop.maxValue) {
            //             color = self.interpolateColor(
            //                 stop.minColor,
            //                 stop.maxColor,
            //                 (value - stop.minValue) / (stop.maxValue - stop.minValue)
            //             );
            //             return false;
            //         }
            //     });
            //     if (!color) {
            //         color = value < gradientInfo.stops[0].minValue ? gradientInfo.belowColor : gradientInfo.aboveColor;
            //     }
            //     return color;
            // });

        return poly;
    }

    /**
     * Draw a line for a data point in the chart ex: Temperature
     * @param chartG base SVG g element this graph will render under
     * @param hourlyData nws data to chart (all properties)
     * @param prop The property in data (sent to 'svgHourGroup') used to create rect
     * @param yScale Scale used vertically to map value 
     * @param options attributes for this element. color, line width, etc 
     */
    svgChartLine(chartG, chartId, hourlyData, prop, yScale, options, chartBounds=null) {
        const self = this;

        const lineEnds = [];
        let isPenDown;
        let pathString = '';

        forEach(hourlyData, (data) => {
            const val = data[prop].value;
            const hours = data[prop].hours;
            let x: number;
            let y: number;
            if (val !== null) {
                x = Math.round(self.xScale(data.unixTime));
                y = Math.round(yScale(val) * 100) / 100;
                pathString += isPenDown ? 'L ' : 'M ';
                pathString += `${x} ${y} `;
                isPenDown = true;
                // 1st and last point of data in the graph
                if (!data.prev || !data.next) {
                    lineEnds.push({x:x, y:y});
                // 1st or last point in segment
                } else if (data.prev[prop].value === null || data.next[prop].value === null) {
                    lineEnds.push({x:x, y:y});
                }
            } else {
                isPenDown = false;
            }
        });

        // Use the same path to generate the fill. But we need to add a couple points to close it off.
        if (options.attrs.fill && chartBounds) {
            const bottomY = yScale(chartBounds.valueBottom);
            const leftX = self.xScale(hourlyData[0].unixTime);
            const endOfPath = `V ${bottomY} H ${leftX} `;
            const fill = chartG.append('path')
                .attr('d', pathString + endOfPath);
            forEach(options.attrs.fill, (attr) => {
                fill.attr(attr.name, attr.value);
            });
        }

        const path = chartG.append('path')
            .attr('fill', 'none')
            .attr('d', pathString);
        forEach(options.attrs.path, (attr) => {
            path.attr(attr.name, attr.value);
        });

        if (options.attrs.circle) {
            const endCircles = chartG.selectAll()
                .data(lineEnds)
                .enter()
                .append('circle')
                .attr('cx', function(d, i) {
                    return d.x;
                })
                .attr('cy', function(d, i) {
                    return d.y;
                });
            // Add the attributes (mostly styling)
            forEach(options.attrs.circle, (attr) => {
                endCircles.attr(attr.name, attr.value);
            });
        }
        // Adds the vale to the hover line on mouse move
        this.addMetricTracking(chartId, prop, yScale, options);
    }

    /**
     * Draw a line for an hour period  ex: Wind Direction
     * @param baseG base SVG g element this graph will render under
     * @param directionProp The property determining the arrow direction
     * @param speedProp The property (speed) where the arrow is centerred
     * @param yScale Scale used vertically to map value 
     * @param options attributes for this element. color, line width, etc 
     * note: wind from - 0:N, 90:E, 180:S, 270:W
     */
    svgHourArrow(baseG, chartId, directionProp, speedProp, yScale, options) {
        const self = this;

        const NORTH = '#00c2ff';    // blue
        const EAST = '#00ffc2';     // green
        const SOUTH = '#ff002c';    // red
        const WEST = '#c200ff';     // purple

        function colorFromDirection(dir) {
            if (dir < 90) {
                return self.interpolateColor(NORTH, EAST, dir / 90);
            } else if (dir < 180) {
                return self.interpolateColor(EAST, SOUTH, (dir - 90) / 90);
            } else if (dir < 270) {
                return self.interpolateColor(SOUTH, WEST, (dir - 180) / 90);
            }
            return self.interpolateColor(WEST, NORTH, (dir - 270) / 90);
        }

        const path = baseG.append('path')
            .attr('transform', function(d) {
                const xC = self.xScale(d.unixTime);
                const yC = yScale(d[speedProp].value);
                const angle = d[directionProp].value
                return `translate(${xC}, ${yC}) rotate(${angle})`
            })
            .attr('stroke', function(d) {
                // console.log('c:', colorFromDirection(d[directionProp].value));
                return colorFromDirection(d[directionProp].value);
            })
            .attr('fill', function(d) {
                return colorFromDirection(d[directionProp].value);
            })
            .attr('d', function (d, i) {
                if (!d[directionProp].hours) {
                    return '';
                }
                const stem = `M 0 -8 L 0 8 `;
                const head = `M -2 2 L 0 8 L 2 2`;
                return stem + head;
            });


            // Add the attributes (mostly styling)
        forEach(options.attrs.path, (attr) => {
            path.attr(attr.name, attr.value);
        });

        // Adds the vale to the hover line on mouse move
        // this.addMetricTracking(chartId, directionProp, yScale, options);
    }

}
