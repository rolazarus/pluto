import { Component, OnInit } from '@angular/core';
import { Hero } from './hero';
import { HeroService } from '../services/hero.service';
import { MessageService } from '../services/message.service';

@Component({
    selector: 'app-heros',
    templateUrl: './heros.component.html',
    styleUrls: ['./heros.component.less']
})
export class HerosComponent implements OnInit {

    heroes: Hero[];
    // selectedHero: Hero;

    constructor(private heroService: HeroService, private messageService: MessageService) {
    }

    ngOnInit() {
        this.getHeroes();
    }

    add(name: string): void {
        name = name.trim();
        if (!name) { return; }
        this.heroService.addHero({ name } as Hero)
            .subscribe(hero => {
                this.heroes.push(hero);
            });
    }
    
    delete(hero: Hero): void {
        this.heroes = this.heroes.filter(h => h !== hero);
        // Note: If you neglect to subscribe(), the service will not send the delete request to the server! As a rule, an Observable does nothing until something subscribes!
        this.heroService.deleteHero(hero).subscribe();
    }

    getHeroes(): void {
        this.heroService.getHeroes()
            .subscribe(heroes => this.heroes = heroes);
    }

    // onSelect(hero: Hero): void {
    //   this.selectedHero = hero;
    //   this.messageService.add('HerosComponent: selected hero' + hero.name);
    // }

}
