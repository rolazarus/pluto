import { Component, OnInit } from '@angular/core';
import { PlutoApiService } from '../services/pluto-api.service';

@Component({
  selector: 'app-pluto',
  templateUrl: './pluto.component.html',
  styleUrls: ['./pluto.component.less']
})
export class PlutoComponent implements OnInit {

  constructor(private plutoApiService: PlutoApiService) { }

    plutoDb;

    ngOnInit() {
        console.log('heros.pluto ngOnInit()');
    }

    async onClickRequestDb(): Promise<any> {
        console.log('Requesting Data from AWS DynamoDB');
        // this.plutoApiService.getPlutoDB();
        await this.plutoApiService.getPlutoDB()
        .then((response) => {
            console.log('getPlutoDB response:', response);
            this.plutoDb = response;
        });

    }

}
