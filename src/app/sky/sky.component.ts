import { Component, Input, OnInit,
    OnChanges, SimpleChanges, SimpleChange
} from '@angular/core';
import * as suncalc from 'suncalc';
import * as d3 from 'd3';
import forEach from 'lodash/forEach';

// import * as moment from 'moment';

// import forEach from 'lodash/forEach';
// import map from 'lodash/map';
// import minBy from 'lodash/minBy';
// import min from 'lodash/min';
// import maxBy from 'lodash/maxBy';
// import max from 'lodash/max';
// import find from 'lodash/find';
// import unionBy from 'lodash/unionBy';

const ATTRS_EARTH = {
    circle: [
        {name: 'fill', value: '#f0f0f0'},
        {name: 'stroke', value: '#e0e0e0'},
        {name: 'stroke-width', value: 1}
    ]
};
const ATTRS_EARTH_RINGS = {
    circle: [
        {name: 'stroke', value: '#e0e0ff'},
        {name: 'stroke-width', value: 1}
    ]
};

const ATTRS_SUN = {
    circle: [
        {name: 'fill', value: '#ffff00'},
        {name: 'stroke', value: '#e0e000'},
        {name: 'stroke-width', value: 1}
    ]
};
const ATTRS_MOON = {
    circle: [
        {name: 'fill', value: '#00ffff'},
        {name: 'stroke', value: '#00e0e0'},
        {name: 'stroke-width', value: 1}
    ],
    text: [
        {name: 'fill', value: '#0000ff'},
        {name: 'font-family', value: 'helvetica'},
        {name: 'font-size', value: '10px'}
    ]
};

@Component({
  selector: 'app-sky-component',
  templateUrl: './sky.component.html',
  styleUrls: ['./sky.component.less']
})

export class SkyComponent implements OnChanges, OnInit {
    @Input() lat: Number;
    @Input() lon: Number;
    @Input() time: any;

    sunTimes;
    sunPosition;
    moonTimes;
    moonPosition;
    moonIllumination;

    /* ********** SVG Stuff ********** */
    discGroup;  // <g>
    sun;        // <circle>
    moon;       // <circle>
    moonText;   // <text>

    svgElementWidth: number = 400;
    svgElementHeight: number = 400;
    svgMargin: any = {
        top: 50,
        right: 50,
        bottom: 50,
        left: 50
    };
    svgWidth: number = this.svgElementWidth - this.svgMargin.left - this.svgMargin.right;
    svgHeight: number = this.svgElementHeight - this.svgMargin.top - this.svgMargin.bottom;
    discRadius: number = this.svgWidth / 2;
    discCenterX: number = this.svgMargin.left + this.discRadius;
    discCenterY: number = this.svgMargin.top + this.discRadius;

    constructor() {
    }

    ngOnInit() {
        console.log('lat:', this.lat);
        console.log('lon:', this.lon);
        if (!this.time) {
            this.time = new Date();
        }
        this.discGroup = this.buildFixedSvgElements();
        this.sun = this.buildSun(this.discGroup);
        this.moon = this.buildMoon(this.discGroup);
        this.getAstronomicalData(this.time, this.lat, this.lon);
        this.positionSun();
        this.positionMoon();
}

    ngOnChanges(changes: SimpleChanges) {
        // 'time' is a JavaScript "Date" object (used by suncalc)
        const timeChange: SimpleChange = changes.time;
        if (timeChange && timeChange.currentValue) {
            const date = timeChange.currentValue;
            // console.log('prev time: ', timeChange.previousValue);
            // console.log('new time: ', timeChange.currentValue);
            this.getAstronomicalData(date, this.lat, this.lon);
            this.positionSun();
            this.positionMoon();
        }
        const lat: SimpleChange = changes.lat;
        if (lat) {
            console.log('prev lat: ', lat.previousValue);
            console.log('new lat: ', lat.currentValue);
        }
        const lon: SimpleChange = changes.lon;
        if (lon) {
            console.log('prev lon: ', lon.previousValue);
            console.log('new lon: ', lon.currentValue);
        }
    }

    getAstronomicalData(date, lat, lon) {
        this.sunTimes = suncalc.getTimes(date, lat, lon);
        this.sunPosition = suncalc.getPosition(date, lat, lon);
        this.moonTimes = suncalc.getMoonTimes(date, lat, lon);
        this.moonPosition = suncalc.getMoonPosition(date, lat, lon);
        this.moonIllumination = suncalc.getMoonIllumination(date, lat, lon);
        // console.log('sunTimes:', this.sunTimes);
        // console.log('sunPosition:', this.sunPosition);
        // console.log('moonTime:', this.moonTimes);
        // console.log('moonPosition:', this.moonPosition);
        // console.log('moonIllumination:', this.moonIllumination);
    }

    /**
     * 
     * @param λ altitude in radians - Angle from horizon (0=horizon, Math.PI/2= straight overhead)
     * @param φ azimuth in radians - Direction south towards west (0=south, Math.PI*3/4=northwest)
     */
    flippedStereographic(λ, φ)  {
        const cosλ = Math.cos(λ);
        const cosφ = Math.cos(φ);
        const k = 1 / (1 + cosλ * cosφ);
        return [
            k * cosφ * Math.sin(λ),
            -k * Math.sin(φ)
        ];
      }

    buildFixedSvgElements() {
        const self = this;

        const svgContainer = d3.select('svg.sky-container');
        const svgGraph = svgContainer.append('g')
            .attr('class', 'disc-group');
            // .attr('transform', function (d, i) {
            //     return `translate(${self.svgMargin.left}, ${self.svgMargin.top})`;
            // });

        const earth = svgGraph.append('circle')
            .attr('r', this.discRadius)
            .attr('cx', this.discCenterX)
            .attr('cy', this.discCenterY);
        forEach(ATTRS_EARTH.circle, (attr) => {
            earth.attr(attr.name, attr.value);
        });
    
        this.buildCircleGrid(svgGraph);

        return svgGraph;
    }

    buildCircleGrid(group) {
        const self = this;

        const DEGREES_MULTIPLIER = Math.PI / 180;
        const PIOVER2 = Math.PI / 2;

        // altitude: 0 = horizon, Math.PI/2 = up
        const circRadians = [
            DEGREES_MULTIPLIER * 10,    // Near Horizon
            DEGREES_MULTIPLIER * 20,
            DEGREES_MULTIPLIER * 30,
            DEGREES_MULTIPLIER * 40,
            DEGREES_MULTIPLIER * 50,
            DEGREES_MULTIPLIER * 60,
            DEGREES_MULTIPLIER * 70,
            DEGREES_MULTIPLIER * 80     // Near vertical
        ];

        const gridCircleGroup = group.selectAll('g.disc-group')
            .data(circRadians)
            .enter()
            .append('g')
            .attr('class', 'grid-circle-group');
        const ring = gridCircleGroup.append('circle')
            .attr('r', function (d, i) {
                const pos = self.flippedStereographic(PIOVER2 - d, 0);
                const r = self.discRadius * pos[0];
                return r;
            })
            .attr('cx', this.discCenterX)
            .attr('cy', this.discCenterY)
            .attr('fill', 'none');
        forEach(ATTRS_EARTH_RINGS.circle, (attr) => {
            ring.attr(attr.name, attr.value);
        });
    
    }

    buildSun(group) {
        const sun = group.append('circle')
            .attr('r', 10)
            .attr('cx', this.discCenterX)
            .attr('cy', this.discCenterY);
        // Add the attributes (mostly styling)
        forEach(ATTRS_SUN.circle, (attr) => {
            sun.attr(attr.name, attr.value);
        });

        return sun;
    }
    buildMoon(group) {
        const moon = group.append('circle')
            .attr('r', 10)
            .attr('cx', this.discCenterX)
            .attr('cy', this.discCenterY);
        forEach(ATTRS_MOON.circle, (attr) => {
            moon.attr(attr.name, attr.value);
        });
    
        const moonText = group.append('text')
            .attr('x', this.discCenterX)
            .attr('y', this.discCenterY)
            .attr('dx', '-.5em')
            .attr('dy', '.35em')
            .text('');
        forEach(ATTRS_MOON.text, (attr) => {
            moonText.attr(attr.name, attr.value);
        });
        this.moonText = moonText;

        return moon;
    }


    positionCelestialBody(celBody, location, illumination =  null) {
        const self = this;
        const PIOVER2 = Math.PI / 2;

        function rnd(val) {
            return Math.round(val * 100) / 100;
        }

        // altitude is distance from horizon. 0=on horizon, PI/2=overhead
        // azimuth is radians. 0 = south -- Math.PI * .75 = northwest
        const altitude = location.altitude;
        let azimuth = location.azimuth;
        azimuth -= Math.PI / 2; // Rotate 90 degrees so south is down
        azimuth = Math.PI - azimuth;
        // Just a reference for debugging
        const angle = altitude * (180 / Math.PI);
        const direction = azimuth * (180 / Math.PI);

        const pos = self.flippedStereographic(PIOVER2 - altitude, 0);
        const h = this.discRadius * pos[0];     // pixels from center
        const x = Math.cos(azimuth) * h;
        const y = Math.sin(azimuth) * h;

        // console.log(`alt:${rnd(altitude)} az:${rnd(azimuth)} -- angle:${rnd(angle)}  direction:${rnd(direction)} -- x:${rnd(x)}, y${rnd(y)}, h:${rnd(h)})`);
        const cx = this.discCenterX + x;
        const cy = this.discCenterY - y;

        celBody
            .attr('cx', cx)
            .attr('cy', cy)
            .style('display', h > self.discRadius ? 'none' : null);

        // TODO: Don't show a number, show the shape (put a circle over this circle?)
        if (illumination) {
            const pct = Math.round(illumination.fraction * 100);
            // console.log('moonIllumination:', pct);
            this.moonText
                .style('display', h > self.discRadius ? 'none' : null)
                .attr('x', cx)
                .attr('y', cy)
                .text(pct);
        }
    }

    positionSun() {
        this.positionCelestialBody(this.sun, this.sunPosition);
    }

    positionMoon() {
        this.positionCelestialBody(this.moon, this.moonPosition, this.moonIllumination);
    }

}

/* **********
        suncalc --- https://github.com/mourner/suncalc
--- getTimes(date, lat, lon))
nadir:         Fri Sep 06 2019 01:14:57 GMT-0400 (Eastern Daylight Time) {}

nightEnd:      Fri Sep 06 2019 05:07:26 GMT-0400 (Eastern Daylight Time) {}
nauticalDawn:  Fri Sep 06 2019 05:43:13 GMT-0400 (Eastern Daylight Time) {}
dawn:          Fri Sep 06 2019 06:17:29 GMT-0400 (Eastern Daylight Time) {}
sunrise:       Fri Sep 06 2019 06:46:16 GMT-0400 (Eastern Daylight Time) {}
sunriseEnd:    Fri Sep 06 2019 06:49:12 GMT-0400 (Eastern Daylight Time) {}
goldenHourEnd: Fri Sep 06 2019 07:23:42 GMT-0400 (Eastern Daylight Time) {}

solarNoon:     Fri Sep 06 2019 13:14:57 GMT-0400 (Eastern Daylight Time) {}

goldenHour:    Fri Sep 06 2019 19:06:11 GMT-0400 (Eastern Daylight Time) {}
sunsetStart:   Fri Sep 06 2019 19:40:41 GMT-0400 (Eastern Daylight Time) {}
sunset:        Fri Sep 06 2019 19:43:38 GMT-0400 (Eastern Daylight Time) {}
dusk:          Fri Sep 06 2019 20:12:24 GMT-0400 (Eastern Daylight Time) {}
nauticalDusk:  Fri Sep 06 2019 20:46:41 GMT-0400 (Eastern Daylight Time) {}
night:         Fri Sep 06 2019 21:22:28 GMT-0400 (Eastern Daylight Time) {}
********** */
