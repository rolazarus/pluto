import { Injectable } from '@angular/core';
import find from 'lodash/find';
import forEach from 'lodash/forEach';
import map from 'lodash/map';
import * as moment from 'moment';
// import { url } from 'inspector';

const ONE_HOUR = 3600000;
const KNOTS_TO_MPH = 1.15078;
const METERS_SEC_TO_MPH = 2.23694;
const MILLIMETERS_TO_INCHES = 0.0393701;
const METERS_TO_FEET = 3.28084;

@Injectable({
    providedIn: 'root'
})
export class NwsApiService {

    ronCache;

    constructor() {
        const self = this;
        caches.open('ron-cache').then(function(cache) {
            console.log('ronCache created');
            self.ronCache = cache; 
        });
    }

    // Information about the specific data point clicked
    locationMetaData;

    unitConversionFunc = {
        'unit:degC': this.celciusToFahrenheight,
        'unit:m_s-1': this.mpsToMph,
        'unit:mm': this.mmToInches,
        'unit:m': this.metersToFeet
    };

    radarTopoShort(siteId) {
        const url = `https://radar.weather.gov/ridge/Overlays/Topo/Long/${siteId}_Topo_Long.jpg`;
        return url;
    }
    radarTopoLong(siteId) {
        const url = `https://radar.weather.gov/ridge/Overlays/Topo/Short/${siteId}_Topo_Short.jpg`;
        return url;
    }

    radarRidgeOverlays(overlay, siteId, size = 'Long') {
        let type, altType;
        if (Array.isArray(overlay)) {
            type = overlay[0];
            altType = overlay[1];
        } else {
            type = overlay;
            altType = overlay;
        }

        const format = type === 'Topo' ? 'jpg' : 'gif';
        // ex: 'https://radar.weather.gov/ridge/Overlays/Topo/Long/BUF_Topo_Long.jpg'
        const imgUrl = `https://radar.weather.gov/ridge/Overlays/${type}/${size}/${siteId}_${altType}_${size}.${format}`;
        return imgUrl;
    }

    radar(siteId, iId = 'N0R') {
        // const iId = 'N0R'; // = Reflectivity
        // const iId = 'N1P'; // = 1 Hour Precipitation
        // const iId = 'NTP'; // = Storm Total Precipitation
        // const iId = 'N0V'; // = Velocity
        // const iId = 'N0S'; // = Storm Relative Motion
        // const iId = 'N0Z'; // = Long Range Reflectivity

        // const url = `https://radar.weather.gov/ridge/Conus/RadarImg/northeast.gif`;  // works with mode: 'no-cors'

        const url = `https://radar.weather.gov/ridge/RadarImg/${iId}/${siteId}_${iId}_0.gif`;  // works with mode: 'no-cors'
        // ^^^ docs for above: https://www.weather.gov/jetstream/ridge_download

        return url;
    }

    async nwsTestApi() {
        let rawResponse;

        const stationId = 'KBUF';

        const url = `https://api.weather.gov/stations/radar/${stationId}`;   // no
        // const url = `https://api.weather.gov/zones`;     //works

        await fetch(url)
            .then((response) => {
                console.log('nwsTestApi() - success - response:', response);
                rawResponse = response;
            });

        return rawResponse;
    }

    async nwsPoints(lat: string, lon: string): Promise<any> {
        const points = `${lat},${lon}`;    // lat, lon
        // Returns metadata about a given latitude/longitude point
        const url = `https://api.weather.gov/points/${points}`;

        let rawResponse;
        let pointProps;
        await fetch(url)
            .then((response) => {
                rawResponse = response;
            });

        await rawResponse.json()
            .then((json) => {
                pointProps = json.properties;
            });

        return pointProps;
    }

    async nwsGridPoints(cwa: string, gridX: number, gridY: number): Promise<any> {
        // properties.cwa=BUF, properties.gridX=40, properties.gridY=38

        const gridLoc = `${cwa}/${gridX},${gridY}`;
        // Returns raw numerical forecast data for a 2.5km grid area
        const url = `https://api.weather.gov/gridpoints/${gridLoc}`;

        let rawResponse;
        let gridProps;

        await fetch(url)
            .then((response) => {
                rawResponse = response;
            });

        await rawResponse.json()
            .then((json) => {
                gridProps = json.properties;
            });

        return gridProps;
    }

    getLocationMetaData() {
        return this.locationMetaData;
    }

    parseNwsTime(time: string) {
        // '2019-03-24T21:00:00+00:00/PT1H' for example
        const parts = time.split('/');
        return {
            tStr: parts[0],
            duration: parts[1],   // PHP dateInterval suffix (PT1H, PT3H, P7DT13H, etc)
        };
    }

    getUnixTime(nwsTime) {
        const time = this.parseNwsTime(nwsTime);
        const gmt = moment(time.tStr).utcOffset(time.tStr);
        const unixTime = gmt.local().valueOf();
        return unixTime;
    }
    getDuration(nwsTime) {
        const time = this.parseNwsTime(nwsTime);
        const duration = moment.duration(time.duration);
        return duration;
    }

    celciusToFahrenheight(celcius) {
        return celcius !== null ? (celcius * 9 / 5) + 32 : null;
    }
    knotsToMph(knots) {
        return knots !== null ? knots * KNOTS_TO_MPH : null;
    }
    mpsToMph(mps) {
        return mps !== null ? mps * METERS_SEC_TO_MPH : null;
    }
    mmToInches(mm) {
        return mm !== null ? mm * MILLIMETERS_TO_INCHES : null;
    }
    metersToFeet(meters) {
        return meters !== null ? meters * METERS_TO_FEET : null;
    }

    /**
     * Add a metric to the master data
     * @param masterData The full object that is being built for graphing
     * @param metricList List of values for this specific metric to add to the master data
     * @param prop Name of the property to add to the master data representing this metric
     */
    addNwsMetric(masterData, metric, prop) {
        const conversionFunc = this.unitConversionFunc[metric.uom];
        const metricList = map(metric.values, (item) => {
            // Get the time, duration, value for each NWS data entry.
            // Not necessarily an entry for each hour since some entries are multiple hours (or null)
            const duration = this.getDuration(item.validTime);
            const valueList = {
                unixTime: this.getUnixTime(item.validTime),
                hourCount: duration.days() * 24 + duration.hours()
            };
            valueList[prop] =  conversionFunc ? conversionFunc(item.value) : item.value;
            return valueList;
        });

        let prevValue = null;
        let repeatHours = 0;
        // For each data point (Hour) in the master data
        forEach(masterData, (item) => {
            // See if the metric has an entry for the same time
            const metricEntry = find(metricList, {unixTime: item.unixTime});
            // If an entry exists, simply add it to the master data
            if (metricEntry) {
                // The entry for this metric that will exists for each hour
                const entry = {
                    value: metricEntry[prop],
                    hours: metricEntry.hourCount
                }
                item[prop] = entry;
                prevValue = metricEntry[prop];
                repeatHours = metricEntry.hourCount - 1;

            }
            // No entry for this hour block, but a previous block says to repeat data
            else if (repeatHours) {
                item[prop] = {
                    value: prevValue,
                    hours: 0
                }
                repeatHours--;
            }
            // No entry for this hour block, and no previous data
            else {
                item[prop] = {
                    value: null,
                    hours: 0
                }
                prevValue = null;
                repeatHours = 0;
            }
        });
    }

    /**
     * Issue request to get the data from NWS
     * @param lat Latitude
     * @param lon Longitude
     */
    async getForcastData(lat: string, lon: string): Promise<any> {
        let pointProps;
        let allWeatherMetrics;

        // await this.nwsTestApi()
        //     .then((response) => {
        //         console.log('getForcastData - nwsTestApi response:', response);
        //     });

        await this.nwsPoints(lat, lon)
            .then((response) => {
                console.log('getForcastData - nwsPoints response:', response);
                pointProps = response;
            });

        this.locationMetaData = pointProps;

        await this.nwsGridPoints(pointProps.cwa, pointProps.gridX, pointProps.gridY)
            .then((response) => {
                console.log('getForcastData - nwsGridPoints response:', response);
                allWeatherMetrics = response;
            });

        let timeStart = this.getUnixTime(allWeatherMetrics.validTimes);
        const duration = this.getDuration(allWeatherMetrics.validTimes);
        let durationMs = duration.asMilliseconds();
        const hourlyData = [];
        while (durationMs > 0) {
            hourlyData.push({
                unixTime: timeStart
            });
            timeStart += ONE_HOUR;
            durationMs -= ONE_HOUR;
        }
        console.log('getForcastData - Number of Hours of data:', hourlyData.length);

        this.addNwsMetric(hourlyData, allWeatherMetrics.temperature, 'tempF');
        this.addNwsMetric(hourlyData, allWeatherMetrics.dewpoint, 'dewpointF');
        this.addNwsMetric(hourlyData, allWeatherMetrics.probabilityOfPrecipitation, 'probPrecipPct');
        this.addNwsMetric(hourlyData, allWeatherMetrics.skyCover, 'skyCoverPct');
        this.addNwsMetric(hourlyData, allWeatherMetrics.relativeHumidity, 'humidityPct');
        this.addNwsMetric(hourlyData, allWeatherMetrics.heatIndex, 'heatIndexF');
        this.addNwsMetric(hourlyData, allWeatherMetrics.windChill, 'windChillF');
        this.addNwsMetric(hourlyData, allWeatherMetrics.windGust, 'windGustMph');
        this.addNwsMetric(hourlyData, allWeatherMetrics.windSpeed, 'windSpeedMph');
        this.addNwsMetric(hourlyData, allWeatherMetrics.windDirection, 'windDirection');
        this.addNwsMetric(hourlyData, allWeatherMetrics.ceilingHeight, 'ceilingHeightFt');
        this.addNwsMetric(hourlyData, allWeatherMetrics.quantitativePrecipitation, 'quantPrecipIn');
        this.addNwsMetric(hourlyData, allWeatherMetrics.snowfallAmount, 'snowfallIn');

        return hourlyData;
    }

}
