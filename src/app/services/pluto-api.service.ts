import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlutoApiService {

  constructor() { }


    // GET on our API gateway / Lambda that talks to DynamoDB
    async getPlutoDB() {
        const url = 'https://08stu76o3a.execute-api.us-east-2.amazonaws.com/dev/plutoDynamoDbCrud?TableName=plutoDynamoDbTable';
        let rawResponse;
        let dbJson;

        await fetch(url)
            .then((response) => {
                rawResponse = response;
            });
        await rawResponse.json()
            .then((json) => {
                dbJson = json;
            });
        return dbJson;
    }

    // OLDgetPlutoDB() {
    //     fetch('https://08stu76o3a.execute-api.us-east-2.amazonaws.com/dev/plutoDynamoDbCrud?TableName=plutoDynamoDbTable')
    //     .then((response) => {
    //         console.log('fetch complete - response:', response);
    //         response.json()
    //         .then((json) => {
    //             console.log('  .json complete - json:', json);
    //             //this.plutoDb = json;
    //         });
    //     });
    //     // fetch('https://08stu76o3a.execute-api.us-east-2.amazonaws.com/dev/plutoDynamoDbCrud?TableName=plutoDynamoDbTable')
    //     // .then(response => response.json())
    //     // .then(json => console.log(json))
    // }
}
