import { TestBed } from '@angular/core/testing';

import { NwsApiService } from './services/nws-api.service';

describe('NwsApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NwsApiService = TestBed.get(NwsApiService);
    expect(service).toBeTruthy();
  });
});
